import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';

class SliverCustomHeaderDelegate extends SliverPersistentHeaderDelegate {
  final dynamic viewModel;
  final double collapsedHeight;
  final double expandedHeight;
  final double paddingTop;
  final String coverImgUrl;
  final String title;
  String statusBarMode = 'dark';

  SliverCustomHeaderDelegate({
    this.viewModel,
    this.collapsedHeight,
    this.expandedHeight,
    this.paddingTop,
    this.coverImgUrl,
    this.title,
  });

  @override
  double get minExtent => this.collapsedHeight + this.paddingTop;

  @override
  double get maxExtent => this.expandedHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

  void updateStatusBarBrightness(shrinkOffset) {
    if (shrinkOffset > 50 && this.statusBarMode == 'dark') {
      this.statusBarMode = 'light';
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
      ));
    } else if (shrinkOffset <= 50 && this.statusBarMode == 'light') {
      this.statusBarMode = 'dark';
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
      ));
    }
  }

  Color makeStickyHeaderBgColor(shrinkOffset) {
    final int alpha = (shrinkOffset / (this.maxExtent - this.minExtent) * 255)
        .clamp(0, 255)
        .toInt();
    return Color.fromARGB(alpha, 255, 255, 255);
  }

  Color makeStickyHeaderTextColor(shrinkOffset, isIcon) {
    if (shrinkOffset <= 50) {
      return isIcon ? Colors.white : Colors.transparent;
    } else {
      // final int alpha = (shrinkOffset / (this.maxExtent - this.minExtent) * 255)
      //     .clamp(0, 255)
      //     .toInt();
      return Colors.purple;
    }
  }

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    this.updateStatusBarBrightness(shrinkOffset);
    final height = MediaQuery.of(context).size.height;
    return Container(
      height: this.maxExtent,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        alignment: Alignment.center,
        fit: StackFit.expand,
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            child: Container(
              color: this.makeStickyHeaderBgColor(shrinkOffset),
              child: SafeArea(
                bottom: false,
                child: Container(
                  height: this.collapsedHeight,
                  child: AppBar(
                    centerTitle: true,
                    automaticallyImplyLeading: false,
                    backgroundColor: this.makeStickyHeaderBgColor(shrinkOffset),
                    elevation: 0,
                    titleSpacing: 0.0,
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(this.title,
                            style: TextStyle(
                                color: this.makeStickyHeaderTextColor(
                                    shrinkOffset, true))),
                      ],
                    ),
                    leading: IconButton(
                      icon: Icon(Icons.notifications,
                          color: this
                              .makeStickyHeaderTextColor(shrinkOffset, true)),
                      onPressed: () {
                        // Navigator.pushNamed(context, u22);
                        viewModel.updateNavigation(u22);
                      },
                    ),
                    actions: <Widget>[
                      IconButton(
                        icon: Icon(Icons.person,
                            color: this
                                .makeStickyHeaderTextColor(shrinkOffset, true)),
                        onPressed: () {
                          Navigator.pushNamed(context, u16);
                        },
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
