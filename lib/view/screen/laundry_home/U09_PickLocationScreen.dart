import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:laundry_delivery_app/view/styles/AppTheme.dart';
import 'package:laundry_delivery_app/viewmodel/GoogleMapViewModel.dart';
import 'package:provider/provider.dart';

class PickLocationScreen extends StatefulWidget {
  PickLocationScreenState createState() => PickLocationScreenState();
}

class PickLocationScreenState extends State<PickLocationScreen> {
  @override
  void initState() {
    super.initState();
    print(
        '==============================PickLocation init==============================');
  }

  @override
  void dispose() {
    print(
        '==============================PickLocation dispose==============================');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<GoogleMapViewModel>(
        builder: (context, googleMapViewModel, _) {
      if (googleMapViewModel.dto.activegps == false) {
        // return ErrorLocationActivate(googleMapViewModel);
        return Scaffold(
            body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(horizontal: 50),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 250,
                  width: 250,
                  child: Image.asset('assets/img/nogps.png'),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'You must activate GPS to get your location',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 40,
                ),
                RaisedButton(
                  onPressed: () {
                    googleMapViewModel.getUserLocation();
                  },
                  child: Text('try again'),
                )
              ],
            ),
          ),
        ));
      } else {
        return Scaffold(
          body: SafeArea(
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Positioned(
                  top: 0,
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: GoogleMap(
                      zoomControlsEnabled: false,
                      mapType: MapType.normal,
                      //markers:provmaps.markers,
                      onCameraMove: googleMapViewModel.onCameraMove,
                      initialCameraPosition: CameraPosition(
                          target: googleMapViewModel.dto.initialposition,
                          zoom: 18.0),
                      onMapCreated: googleMapViewModel.onGoogleMapCreated,
                      onCameraIdle: () async {
                        googleMapViewModel.getMoveCamera();
                      },
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).padding.top),
                    child: SizedBox(
                      width: AppBar().preferredSize.height,
                      height: AppBar().preferredSize.height,
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          borderRadius: BorderRadius.circular(
                              AppBar().preferredSize.height),
                          child: Icon(
                            Icons.arrow_back_ios,
                            color: AppTheme.nearlyBlack,
                          ),
                          onTap: () {
                            Navigator.pop(context, false);
                          },
                        ),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).padding.top, right: 10),
                    child: SizedBox(
                      width: AppBar().preferredSize.height,
                      height: AppBar().preferredSize.height,
                      child: FloatingActionButton(
                        onPressed: googleMapViewModel.getUserLocation,
                        backgroundColor: Colors.white,
                        child: Icon(
                          Icons.gps_fixed,
                          color: Colors.purple,
                        ),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment(0.0, -0.4),
                  child: Container(
                    alignment: Alignment.center,
                    height: 150,
                    margin: EdgeInsets.symmetric(horizontal: 70),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                          BorderRadius.circular(3), //border corner radius
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5), //color of shadow
                          spreadRadius: 1, //spread radius
                          blurRadius: 3, // blur radius
                          offset: Offset(0, 2), // changes position of shadow
                          //first paramerter of offset is left-right
                          //second parameter is top to down
                        ),
                        //you can set more BoxShadow() here
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding:
                              EdgeInsets.only(top: 10, left: 10, right: 10),
                          alignment: Alignment.center,
                          child: TextField(
                            maxLines: 2,
                            controller:
                                googleMapViewModel.dto.locationController,
                            decoration:
                                InputDecoration(border: InputBorder.none),
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.w400),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                          child: RaisedButton(
                            child: Text('เลือกที่อยู่'),
                            color: Colors.green.shade700,
                            textColor: Colors.white,
                            onPressed: () {
                              googleMapViewModel.setLocationOnClick();
                              Navigator.pop(context, true);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Icon(
                    Icons.location_on,
                    size: 50,
                    color: Colors.redAccent,
                  ),
                ),
              ],
            ),
          ),
        );
      }
    });
  }
}
