import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:laundry_delivery_app/viewmodel/viewModel.dart';
import 'package:location/location.dart';

class PermisstionLocationModel {
  Location _location;
  Geolocator _geolocator;
  DtoPermissionLocation _dto;

  PermisstionLocationModel._privateConstructor() {
    this._location = Location();
    this._geolocator = new Geolocator();
    this._dto = new DtoPermissionLocation();
  }
  static final PermisstionLocationModel instance =
      PermisstionLocationModel._privateConstructor();

  Future<void> checkPermissions() async {
    final PermissionStatus permissionGrantedResult =
        await this._location.hasPermission();
    this._dto.permissionGranted = permissionGrantedResult;
  }

  Future<void> requestPermission(
      PermissionLocationViewmodel permissionLocationViewmodel) async {
    this._dto.errorMessage = 'permission request success';
    this._dto.permissionGranted = await this._location.hasPermission();
    print('hasPermission: =========' + this._dto.permissionGranted.toString());
    if (this._dto.permissionGranted == PermissionStatus.denied) {
      this._dto.permissionGranted = await this._location.requestPermission();
      print('requestPermission: =========' +
          this._dto.permissionGranted.toString());
      if (this._dto.permissionGranted != PermissionStatus.granted) {
        this._dto.errorMessage = 'permission request fail';
        permissionLocationViewmodel.setDto(this._dto);
        return;
      }
    }

    bool _serviceEnabled = await this._location.serviceEnabled();
    print('serviceEnabled: =========' + _serviceEnabled.toString());
    if (!_serviceEnabled) {
      _serviceEnabled = await this._location.requestService();
      print('requestService: =========' + _serviceEnabled.toString());
      if (!_serviceEnabled) {
        this._dto.errorMessage = 'permission request fail';
        permissionLocationViewmodel.setDto(this._dto);
        return;
      }
    }
    //ยังมีปัญหาตอนครั้งแรก
    // this._dto.locationData = await this._location.getLocation();
    // แก้ไขด้วยการใส่ exception
    try {
      this._dto.locationData = await this._location.getLocation();
    } on PlatformException {
      this._dto.errorMessage = 'permission request fail';
    } catch (e) {
      // can't get location, could be disabled!!!!
      this._dto.errorMessage = 'permission request fail';
      print(e);
    }
    permissionLocationViewmodel.setDto(this._dto);
  }

  Future<void> requestLocation() async {
    await this._location.getLocation();
  }

  Future<bool> serviceEnabled() async {
    return await this._location.serviceEnabled();
  }

  void clearData() {
    print(
        '======************************===========clearData===========************************=======');
    this._dto.errorMessage = '';
    this._dto.locationData = null;
    this._dto.messageLocation = '';
  }
}

class DtoPermissionLocation {
  String errorMessage;
  LocationData locationData;
  PermissionStatus permissionGranted;
  String messageLocation;
}
