import 'dart:convert';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:laundry_delivery_app/data/entity/store/Store.dart';
import 'package:laundry_delivery_app/data/entity/store/StoreResponse.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/viewmodel/ViewModel.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'services/service.dart';

class LaundryStoreModel {
  DtoLaundry dto;
  Service _service;
  String _accessToken;

  LaundryStoreModel._privateConstructor() {
    this.dto = new DtoLaundry();
    this._service = new Service();
    SharedPreferences.getInstance().then((value) {
      this._accessToken = value.getString('accessToken') ?? '';
      this.dto.lattitude = value.getDouble('latitude') ?? 0;
      this.dto.longitude = value.getDouble('longitude') ?? 0;
    });
  }

  static final LaundryStoreModel instance =
      LaundryStoreModel._privateConstructor();

  prepareData(LaundryStoreViewModel laundryStoreViewModel) async {
    print('---------------------------------------------------getListStore');
    print(this._accessToken);
    print(this.dto.lattitude);
    print(this.dto.longitude);

    dynamic body = jsonEncode(<String, dynamic>{
      'latitude': this.dto.lattitude,
      'longitude': this.dto.longitude,
    });
    print(body.toString());
    this.dto.storeResponse = StoreResponse.fromJson(
        await _service.postMock(url_customer_store_list, body, _accessToken));
    if (this.dto.storeResponse != null) {
      if (this.dto.storeResponse.code == 0) {
        this.dto.errorMessage = "success";
        laundryStoreViewModel.setDto(this.dto);
      } else {
        this.dto.errorMessage = "error";
        laundryStoreViewModel.setDto(this.dto);
      }
    } else {
      this.dto.errorMessage = "error";
      laundryStoreViewModel.setDto(this.dto);
    }
  }

  void setStoreDetail(Store data) {
    this.dto.store = data;
  }

  void setCurrentLocation(
      //set from u09 pick location
      {String currentAddress,
      LatLng currentLatLng,
      LocationData locationData}) {
    this.dto.currentAddress = currentAddress;
    if (currentLatLng != null) {
      this.dto.currentLatLng = currentLatLng;
    }
    if (locationData != null) {
      this.dto.currentLatLng =
          new LatLng(locationData.latitude, locationData.longitude);
    }
  }

  get Dto {
    return this.dto;
  }
}

class DtoLaundry {
  String errorMessage;
  String currentAddress;
  LatLng currentLatLng;
  double lattitude;
  double longitude;
  StoreResponse storeResponse;
  Store store;

  DtoLaundry();
}
