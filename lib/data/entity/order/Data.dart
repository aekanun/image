class Data {
  int orderNo;

  Data({this.orderNo});

  Data.fromJson(Map<String, dynamic> json) {
    orderNo = json['order_no'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order_no'] = this.orderNo;
    return data;
  }

  void clear() {
    this.orderNo = null;
  }
}
