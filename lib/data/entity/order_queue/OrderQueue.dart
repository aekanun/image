import 'Data.dart';

class OrderQueue {
  int code;
  String msgEn;
  String msgTh;
  List<Data> data;

  OrderQueue({this.code, this.msgEn, this.msgTh, this.data});

  OrderQueue.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    msgEn = json['msg_en'];
    msgTh = json['msg_th'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['msg_en'] = this.msgEn;
    data['msg_th'] = this.msgTh;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
