import 'package:flutter/material.dart';

class DialogUtil {
  DialogUtil();

  showAlertDialogLaundryStore(
      BuildContext context, String ok, String title, String content) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text(ok),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(content),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
