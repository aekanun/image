import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/utility/ui_helper.dart';
import 'package:laundry_delivery_app/view/styles/ColorsConst.dart';
import 'package:laundry_delivery_app/viewmodel/viewModel.dart';
import 'package:provider/provider.dart';
import 'package:settings_ui/settings_ui.dart';

class SettingScreen extends StatefulWidget {
  SettingScreenState createState() => SettingScreenState();
}

class SettingScreenState extends State<SettingScreen> {
  @override
  void initState() {
    super.initState();
    print(
        '==============================Setting init==============================');
  }

  @override
  void dispose() {
    print(
        '==============================Setting dispose==============================');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;
    return ChangeNotifierProvider(
        create: (context) => SettingViewModel.instance(),
        child: Consumer<SettingViewModel>(
          builder: (context, setting, _) {
            return Scaffold(
              appBar: AppBar(title: Text('Setting'),backgroundColor: ColorsConst.basePurple,),
              body: SettingsList(
                backgroundColor: Colors.white,
                sections: [
                  CustomSection(
                    child: Container(
                      height: 124.0,
                      margin: new EdgeInsets.all(8.0),
                      decoration: new BoxDecoration(
                        //color: new Color(0xFF333366),
                        gradient: LinearGradient(
                          colors: [
                            Colors.purple.shade400,
                            Colors.purple.shade900,
                          ],
                        ),
                        shape: BoxShape.rectangle,
                        borderRadius: new BorderRadius.circular(8.0),
                        boxShadow: <BoxShadow>[
                          new BoxShadow(
                            color: Colors.black12,
                            blurRadius: 10.0,
                            offset: new Offset(0.0, 10.0),
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                        child: Row(children: <Widget>[
                          setting.dto.profileFacebook != null
                              ? Image.network(
                                  setting.dto.profileFacebook.picture_url)
                              : Icon(Icons.person),
                          SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(setting.dto.profileFacebook != null
                                  ? setting.dto.profileFacebook.name
                                  : ''),
                              Text(setting.dto.profileFacebook != null
                                  ? setting.dto.profileFacebook.email
                                  : ''),
                            ],
                          ),
                        ]),
                      ),
                    ),
                  ),
                  SettingsSection(
                    title: 'general',
                    // titleTextStyle: TextStyle(fontSize: 30),
                    tiles: [
                      SettingsTile(
                        title: 'Language/ภาษา',
                        subtitle: 'language',
                        leading: Icon(Icons.language),
                        onTap: () {},
                      ),
                      SettingsTile(
                        title: 'Phone number',
                        leading: Icon(Icons.phone),
                        onTap: () => {
                          // Navigator.pushNamed(context, u17),
                        },
                      ),
                      SettingsTile(
                        title: 'History',
                        leading: Icon(Icons.history),
                        onTap: () => {
                          Navigator.pushNamed(context, u17),
                        },
                      ),
                      SettingsTile(
                        title: 'Watch tutorial',
                        leading: Icon(Icons.tv),
                        onTap: () => {
                          Navigator.pushNamed(
                            context,
                            u20,
                          ),
                        },
                      ),
                    ],
                  ),
                  SettingsSection(
                    title: 'Law and policy',
                    tiles: [
                      SettingsTile(
                        title: 'Terms and conditions of service',
                        onTap: () => {},
                      ),
                    ],
                  ),
                  CustomSection(
                    child: Visibility(
                      // visible: widget.isLogin,
                      child: Container(
                        alignment: Alignment.bottomCenter,
                        child: SingleChildScrollView(
                            physics: AlwaysScrollableScrollPhysics(),
                            padding: EdgeInsets.symmetric(
                              horizontal: 40.0,
                            ),
                            child: Column(
                              children: <Widget>[
                                InkWell(
                                  child: Container(
                                    alignment: Alignment.center,
                                    constraints:
                                        BoxConstraints.expand(height: 45),
                                    child: Text("Logout",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.purple)),
                                    margin: EdgeInsets.only(bottom: 36),
                                  ),
                                  onTap: () {
                                    setting.logout();
                                    Navigator.pushNamed(context, u01);
                                  },
                                ),
                              ],
                            )),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ));
  }
}
