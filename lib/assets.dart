class Assets {
  static const String _images = 'assets/img/';

  static const String nogps = '${_images}nogps.png';
  static const String paymentType_cash = '${_images}cash.png';
  static const String check = '${_images}check.png';
  static const String delivery = '${_images}delivery_services.json';
  static const String delivery_from_network =
      'https://assets2.lottiefiles.com/packages/lf20_jmejybvu.json';
  static const String order_queue = '${_images}order_queue.json';
  static const String order_queue_from_network =
      'https://assets5.lottiefiles.com/packages/lf20_lPLqHd.json';
  static const String location = '${_images}location.json';
  static const String location_from_network =
      'https://assets9.lottiefiles.com/packages/lf20_3mirMw.json';
}
