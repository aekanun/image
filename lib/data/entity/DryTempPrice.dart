class DryTempPrice {
  String temp;
  int price;

  DryTempPrice({String temp, int price});

  DryTempPrice.fromJson(Map<String, dynamic> json) {
    temp = json['temp'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['temp'] = this.temp;
    data['price'] = this.price;
    return data;
  }
}
