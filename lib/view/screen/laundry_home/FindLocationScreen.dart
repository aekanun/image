import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/assets.dart';
import 'package:laundry_delivery_app/viewmodel/viewModel.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import 'U08_LaundryHomeStoreScreen.dart';

class FindLocationScreen extends StatefulWidget {
  FindLocationState createState() => FindLocationState();
}

class FindLocationState extends State<FindLocationScreen> {
  @override
  void initState() {
    super.initState();
    // PermissionLocationViewmodel permission =
    //     Provider.of<PermissionLocationViewmodel>(context, listen: false);
    // permission.requestPermission();
    print(
        '==============================FindLocation init==============================');
  }

  @override
  void dispose() {
    print(
        '==============================FindLocation dispose==============================');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ChangeNotifierProvider(
        create: (context) => PermissionLocationViewmodel.instance(),
        child: Consumer<PermissionLocationViewmodel>(
          builder: (context, permission, _) {
            if (permission.dto.errorMessage != null &&
                permission.dto.errorMessage != '') {
              return LaundryHomeStoreScreen();
            } else {
              return Center(
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 16, bottom: 8, right: 16, top: 8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        permission.dto.messageLocation != null
                            ? 'จัดส่งที่'
                            : 'ค้นหาที่อยู่',
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(height: 30),
                      Lottie.asset(
                        Assets.location,
                        fit: BoxFit.fill,
                      ),
                      SizedBox(height: 10),
                      permission.dto.errorMessage != null &&
                              permission.dto.errorMessage ==
                                  'permission request success'
                          ? Text(
                              permission.dto.messageLocation != null
                                  ? permission.dto.messageLocation
                                  : '',
                              style: TextStyle(fontSize: 16),
                            )
                          : CircularProgressIndicator(),
                    ],
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
