import 'dart:async';

import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/model/AuthenModel.dart';
import 'package:laundry_delivery_app/model/LaundryStoreModel.dart';
import 'package:laundry_delivery_app/model/model.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/utility/enums.dart';

class AuthenViewModel with ChangeNotifier {
  DtoAuthen dto;
  //String currentNavigation = "";
  LoginState loginState = LoginState.none;

  AuthenModel _authenModel;
  RegisterModel _registerModel;
  LaundryStoreModel _laundryStoreModel;

  AuthenViewModel();
  AuthenViewModel.instance() {
    this._authenModel = AuthenModel.instance;
    this._registerModel = RegisterModel.instance;
    this._laundryStoreModel = LaundryStoreModel.instance;
    this.dto = new DtoAuthen();
  }

  // void initViewModel() {
  //   removeListener(() {
  //     this.dto;
  //   });
  //   addListener(() {
  //     this.dto;
  //   });
  // }

  Future<void> loginWithUser(Map mapController) async {
    this.loginState = LoginState.user;
    await _authenModel.setLoginData(mapController, this);
  }

  Future<void> loginWithFacebook() async {
    this.loginState = LoginState.facebook;
    await _authenModel.loginWithFacebook(this);
  }

  Future<void> loginWithGmail() async {
    this.loginState = LoginState.google;
    await _authenModel.loginWithGmail(this);
  }

  Future<void> logoutWithGmail() async {
    await _authenModel.logoutWithGmail();
  }

  get getNavigation {
    return this._authenModel.getDto;
  }

  void setNavigation(String navigation) {
    this._authenModel.updateNavigation(navigation, this);
  }

  setDto(DtoAuthen dto) {
    this.dto = dto;
    switch (this.loginState) {
      case LoginState.none:
        break;
      case LoginState.facebook:
        if (this.dto.errorMessage == "login success") {
          this._authenModel.updateNavigation(u08_0, this);
        } else if (this.dto.errorMessage == "social login fail") {
          this
              ._registerModel
              .registerFromSocialLogin('facebook', this.dto.authen.token);
          this._authenModel.updateNavigation(u04, this);
        } else {
          //social login connect fail
        }
        break;
      case LoginState.google:
        if (this.dto.errorMessage == "login success") {
          this._authenModel.updateNavigation(u08_0, this);
        } else if (this.dto.errorMessage == "social login fail") {
          this
              ._registerModel
              .registerFromSocialLogin('google', this.dto.authen.token);
          this._authenModel.updateNavigation(u04, this);
        } else {
          //social login connect fail
        }
        break;
      case LoginState.user:
        if (this.dto.errorMessage == "login success") {
          this._authenModel.updateNavigation(u08_0, this);
        } else if (this.dto.errorMessage == "social login fail") {
          //show dialog error
          // this._authenModel.updateNavigation(u05, this);
        }
        break;
    }
    //  notifyListeners();
  }

  void updateNavigation(DtoAuthen dto) {
    print(
        '===========================================authen update navigation');
    this.dto = dto;
    notifyListeners();
  }

  // Widget get getNavigation {
  //   if (currentNavigation == u03) {
  //     return UserSignInScreen(viewModel: this);
  //   } else if (currentNavigation == u08) {
  //     return LaundryHomeStoreScreen();
  //   } else if (currentNavigation == u08_0) {
  //     if (this._laundryStoreModel.dto.lattitude != 0 &&
  //         this._laundryStoreModel.dto.longitude != 0) {
  //       return LaundryHomeStoreScreen();
  //     }
  //     return FindLocationScreen();
  //   } else {
  //     return SignInScreen();
  //   }
  // }

  // void updateNavigation(String navigation) {
  //   currentNavigation = navigation;
  //   notifyListeners();
  // }
}
