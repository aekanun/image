import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/view/widget/ProgressHUD.dart';
import 'package:laundry_delivery_app/viewmodel/RegisterViewModel.dart';
import 'package:provider/provider.dart';

class VerifyPhoneNumberScreen extends StatefulWidget {
  VerifyPhoneNumberScreenState createState() => VerifyPhoneNumberScreenState();
}

class VerifyPhoneNumberScreenState extends State<VerifyPhoneNumberScreen> {
  bool _isLoading = false;
  final _teCountryCode = TextEditingController();
  final _teMobileNumber = TextEditingController();

  FocusNode _focusNodeFirstName = FocusNode();

  @override
  void dispose() {
    _teCountryCode.dispose();
    _teMobileNumber.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _teCountryCode.text = "66";
    print('VerifyPhoneNumberScreen initState');
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<RegisterViewModel>(builder: (context, register, _) {
      return Scaffold(
        backgroundColor: Color(0xFFF1F1EF),
        body: ProgressHUD(
          child: Container(
            height: double.infinity,
            child: SingleChildScrollView(
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      alignment: FractionalOffset.center,
                      margin: EdgeInsets.fromLTRB(10.0, 150.0, 10.0, 0.0),
                      padding: EdgeInsets.fromLTRB(10.0, 50.0, 10.0, 100.0),
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(255, 255, 255, 1.0),
                        border: Border.all(color: Color(0x33A6A6A6)),
                        borderRadius: BorderRadius.all(Radius.circular(6.0)),
                      ),
                      child: Column(
                        children: <Widget>[
                          TextField(
                            decoration: InputDecoration(hintText: "Country"),
                            controller: _teCountryCode,
                          ),
                          TextField(
                            decoration:
                                InputDecoration(hintText: "Mobile Number"),
                            controller: _teMobileNumber,
                            focusNode: _focusNodeFirstName,
                            keyboardType: TextInputType.number,
                          ),
                        ],
                      ),
                    ),
                    RaisedButton(
                      color: Color(0xFFFFA600),
                      onPressed: () {
                        if (_teCountryCode.text.isNotEmpty &&
                            _teMobileNumber.text.isNotEmpty) {
                          register.setMobilePhoneNumber(_teMobileNumber.text);
                        }
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(40))),
                      child: Text(
                        "confirm",
                        style: TextStyle(
                          fontSize: 18,
                          color: Color(0xFFFFFFFF),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          inAsyncCall: _isLoading,
        ),
      );
    });
  }
}
