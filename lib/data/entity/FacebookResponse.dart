import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class FacebookResponse {
  // String access_token;
  // String userId;
  // DateTime expires;
  // List<String> permissions;
  // List<String> declinedPermissions;

  FacebookAccessToken accessToken;

  FacebookResponse(this.accessToken);

  factory FacebookResponse.fromMap(FacebookLoginResult result) {
    if (result == null) return null;
    return FacebookResponse(result.accessToken);
    // FacebookAccessToken accessToken;
    // switch (result.status) {
    //   case FacebookLoginStatus.loggedIn:
    //     accessToken = result.accessToken;
    //     break;
    //   case FacebookLoginStatus.cancelledByUser:
    //     accessToken = null;
    //     break;
    //   case FacebookLoginStatus.error:
    //     accessToken = null;
    //     break;
    // }
  }

  FacebookAccessToken get getAccessToken => accessToken;

  // String get getAccessToken => access_token;

  // String get getUserId => userId;

  // DateTime get getExpires => expires;

  // List<String> get getPermissions => permissions;

  // List<String> get getDeclinedPermissions => declinedPermissions;

  // set accessToken(String access_token) {
  //   access_token = access_token;
  // }
}
