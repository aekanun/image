class OrderQueueInfo {
  OrderQueueInfo({
    // this.id,
    // this.date,
    this.deliveryProcesses,
  });

  // final int id;
  // final DateTime date;
  final List<DeliveryProcess> deliveryProcesses;
}

class DeliveryProcess {
  DeliveryProcess(
    this.name, {
    this.status,
    this.messages = const [],
  });

  DeliveryProcess.complete()
      : this.name = 'Done',
        this.status = false,
        this.messages = const [];

  final String name;
  final bool status;
  final List<DeliveryMessage> messages;

  // bool get isCompleted => name == 'Done';
  bool get isCompleted => status == true;
}

class DeliveryMessage {
  DeliveryMessage(this.createdAt, this.message);

  final String createdAt; // final DateTime createdAt;
  final String message;

  @override
  String toString() {
    return '$createdAt $message';
  }
}
