import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:laundry_delivery_app/data/entity/order_queue/OrderQueue.dart';
import 'package:laundry_delivery_app/data/entity/order_queue/OrderQueueInfo.dart';
import 'package:laundry_delivery_app/model/services/service.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/viewmodel/ViewModel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class OrderQueueModel {
  String _accessToken;
  Service _service;
  int orderIndex = 0;
  BitmapDescriptor merchantMarker;
  BitmapDescriptor userMarker;
  DtoOrderQueue _dto;

  OrderQueueModel._privateConstructor() {
    SharedPreferences.getInstance().then((value) {
      this._accessToken = value.getString('accessToken') ?? '';
    });
    this._dto = new DtoOrderQueue();
    this._dto.title = "ออเดอร์ สั่งซื้อ";
    this._dto.scfdRootKey = new GlobalKey<ScaffoldState>();
    this._service = new Service();
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(size: Size(48, 48)), 'assets/img/merchant.png')
        .then((onValue) {
      this.merchantMarker = onValue;
    });
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(size: Size(48, 48)), 'assets/img/user.png')
        .then((onValue) {
      this.userMarker = onValue;
    });
  }

  static final OrderQueueModel instance = OrderQueueModel._privateConstructor();

  void getDto(OrderQueueViewModel orderQueueViewModel) {
    orderQueueViewModel.setInitial(this._dto);
  }

  void setIndexOrderTrackingTimeLine(
      int index, OrderQueueViewModel orderQueueViewModel) {
    this.orderIndex = index;
    onAddMarker(
        "userId",
        new LatLng(
            this
                ._dto
                .orderQueue
                .data[this.orderIndex]
                .customerDetail
                .location
                .latitude,
            this
                ._dto
                .orderQueue
                .data[this.orderIndex]
                .customerDetail
                .location
                .longitude),
        this._dto.orderQueue.data[this.orderIndex].customerDetail.name,
        this.userMarker);

    onAddMarker(
        "merchantId",
        new LatLng(
            this
                ._dto
                .orderQueue
                .data[this.orderIndex]
                .merchantDetail
                .location
                .latitude,
            this
                ._dto
                .orderQueue
                .data[this.orderIndex]
                .merchantDetail
                .location
                .longitude),
        this._dto.orderQueue.data[this.orderIndex].merchantDetail.name,
        this.merchantMarker);
    this._dto.orderQueueInfo = orderInfo();
    orderQueueViewModel.setOrderQueue(this._dto);
  }

  Future<void> getOrderQueue(OrderQueueViewModel orderQueueViewModel) async {
    this._dto.orderQueue = OrderQueue.fromJson(
        await this._service.get(url_order_queue, this._accessToken));
    if (this._dto.orderQueue != null) {
      if (this._dto.orderQueue.code == 0) {
        this._dto.errorMessage = "success";
        orderQueueViewModel.setOrderQueue(this._dto);
      } else {
        this._dto.errorMessage = "error";
        orderQueueViewModel.setOrderQueue(this._dto);
      }
    } else {
      this._dto.errorMessage = "error";
      orderQueueViewModel.setOrderQueue(this._dto);
    }
  }

  Future<void> callFromUrl(
      String url, OrderQueueViewModel orderQueueViewModel) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      orderQueueViewModel.setErrorMessageFromCall('Could not launch $url');
    }
  }

  void onAddMarker(
      String markerId, LatLng pos, String title, BitmapDescriptor icon) {
    this._dto.markers.add(Marker(
          // This marker id can be anything that uniquely identifies each marker.
          markerId: MarkerId(markerId),
          position: pos,
          infoWindow: InfoWindow(
            title: title != null ? title : "",
          ),
          icon: icon,

          // onTap: () {
          //   print("click marker:");
          //   print(title);
          //   print(pos);
          // },
        ));
  }

  void onRemoveMarker() {
    this
        ._dto
        .markers
        .removeWhere((element) => element.mapsId.value == 'userId');
    this
        ._dto
        .markers
        .removeWhere((element) => element.mapsId.value == 'merchantId');
  }

  OrderQueueInfo orderInfo() => OrderQueueInfo(
        // id: 1,
        // date: DateTime.now(),
        deliveryProcesses: [
          DeliveryProcess(
            'Confirmed order',
            status: this._dto.orderQueue.data[this.orderIndex].orderStatus >= 1
                ? true
                : false,
            messages: [
              DeliveryMessage('', 'Reached halfway marker'),
            ],
          ),
          DeliveryProcess(
            'The driver is coming to you',
            status: this._dto.orderQueue.data[this.orderIndex].orderStatus >= 2
                ? true
                : false,
            messages: [
              DeliveryMessage('', 'Driver arrived at destination'),
            ],
          ),
          DeliveryProcess(
            'Picking up and going to the store',
            status: this._dto.orderQueue.data[this.orderIndex].orderStatus >= 3
                ? true
                : false,
            messages: [
              DeliveryMessage('', 'Driver picked up and going to the store'),
            ],
          ),
          DeliveryProcess(
            'Received and Laundry process',
            status: this._dto.orderQueue.data[this.orderIndex].orderStatus >= 4
                ? true
                : false,
            messages: [
              DeliveryMessage('', 'Hand in clothes at the storen'),
              DeliveryMessage('', 'Washing'),
              DeliveryMessage('', 'Drying'),
            ],
          ),
          DeliveryProcess(
            'Ready to pick up',
            status: this._dto.orderQueue.data[this.orderIndex].orderStatus >= 5
                ? true
                : false,
            messages: [
              DeliveryMessage('', 'Finished washing'),
            ],
          ),
          DeliveryProcess(
            'The driver is going to the store',
            status: this._dto.orderQueue.data[this.orderIndex].orderStatus >= 6
                ? true
                : false,
            messages: [
              DeliveryMessage('', 'The driver is going to the store'),
            ],
          ),
          DeliveryProcess(
            'Picking up and coming to you',
            status: this._dto.orderQueue.data[this.orderIndex].orderStatus >= 7
                ? true
                : false,
            messages: [
              DeliveryMessage('', 'Picking up and coming to you'),
            ],
          ),
          DeliveryProcess(
            'Arrived',
            status: this._dto.orderQueue.data[this.orderIndex].orderStatus >= 8
                ? true
                : false,
            messages: [
              DeliveryMessage('', 'Driver arrived at destination'),
              DeliveryMessage('', 'Package delivered'),
            ],
          ),
        ],
      );
}

class DtoOrderQueue {
  String errorMessage;
  String title;
  var scfdRootKey;
  OrderQueue orderQueue;
  OrderQueueInfo orderQueueInfo;
  Set<Marker> markers = Set<Marker>();
  DtoOrderQueue();
}
