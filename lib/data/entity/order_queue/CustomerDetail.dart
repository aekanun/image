import 'Location.dart';

class CustomerDetail {
  String name;
  String address;
  Location location;
  String phone;

  CustomerDetail({this.name, this.address, this.location, this.phone});

  CustomerDetail.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    address = json['address'];
    location = json['location'] != null
        ? new Location.fromJson(json['location'])
        : null;
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['address'] = this.address;
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    data['phone'] = this.phone;
    return data;
  }
}