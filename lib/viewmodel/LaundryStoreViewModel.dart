import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:laundry_delivery_app/data/entity/store/Store.dart';
import 'package:laundry_delivery_app/model/GoogleMapModel.dart';
import 'package:laundry_delivery_app/model/LaundryStoreModel.dart';

class LaundryStoreViewModel with ChangeNotifier {
  DtoLaundry dto;

  String currentNavigation = "";
  final currentPageNotifier = ValueNotifier<int>(0);
  BitmapDescriptor iconMarker;
  double cameraZoom = 15;
  GoogleMapController _mapController;
  final Set<Marker> _markers = Set();
  Set<Marker> get markers => _markers;
  GoogleMapController get mapController => _mapController;
  // LoadingStatus loadingStatus = LoadingStatus.none;

  LaundryStoreModel _laundryStoreModel;
  GoogleMapModel _googleMapModel;

  ScrollController controller;
  // var keyScaffold = new GlobalKey<ScaffoldState>();

  LaundryStoreViewModel();
  LaundryStoreViewModel.instance() {
    this._laundryStoreModel = LaundryStoreModel.instance;
    this._googleMapModel = GoogleMapModel.instance;
    this.dto = new DtoLaundry();
    //for laundry store detail
    // this.iconMarker = BitmapDescriptor.fromAsset("assets/img/location_pin.png");
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(size: Size(48, 48)), 'assets/img/merchant.png')
        .then((onValue) {
      this.iconMarker = onValue;
    });
    this.controller = new ScrollController();
    getListStore();
  }

  Future<void> getListStore() async {
    await _laundryStoreModel.prepareData(this);
  }

  void updateData() {
    notifyListeners();
  }

  setStoreIndex(Store data) {
    this._laundryStoreModel.setStoreDetail(data);
    print(data.name);
    print(data.address);
  }

  setDto(DtoLaundry dto) {
    this.dto = dto;
    notifyListeners();
  }

  void updateNavigation(String navigation) {
    currentNavigation = navigation;
    notifyListeners();
  }

  void updateCurrentPage(int val) {
    this.currentPageNotifier.value = val;
    notifyListeners();
  }

  //for laundry store detail
  void onCreated(GoogleMapController controller) {
    if (_mapController == null) {
      _mapController = controller;
    }
    notifyListeners();
  }

  void addMarker(LatLng location, String name, String address) {
    _markers.add(Marker(
        markerId: MarkerId(location.toString()),
        position: location,
        infoWindow: InfoWindow(title: name, snippet: address),
        icon: this.iconMarker));
    notifyListeners();
  }
}
