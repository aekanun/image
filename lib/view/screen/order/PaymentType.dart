import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/assets.dart';
import 'package:laundry_delivery_app/view/styles/AppTheme.dart';
import 'package:laundry_delivery_app/view/styles/ColorsConst.dart';
import 'package:laundry_delivery_app/viewmodel/OrderViewModel.dart';

class PaymentType extends StatelessWidget {
  OrderViewModel viewModel;

  PaymentType({this.viewModel});
  @override
  Widget build(BuildContext context) {
    return Container(
        color: AppTheme.nearlyWhite,
        child: Scaffold(
            backgroundColor: ColorsConst.white,
            appBar: AppBar(
              // key: attr.scfdRootKey,
              title: Text(
                'Select Payment Method',
                style: TextStyle(color: Colors.black),
              ),
              bottomOpacity: 0.0,
              elevation: 0.0,
              backgroundColor: ColorsConst.white,
              leading: new IconButton(
                  icon: new Icon(Icons.arrow_back),
                  color: Colors.black,
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ),
            body: Stack(
              children: <Widget>[
                Column(children: <Widget>[
                  Positioned(
                    top: (MediaQuery.of(context).size.width / 1.2) - 24.0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Container(
                      decoration: BoxDecoration(
                        color: AppTheme.nearlyWhite,
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: AppTheme.grey.withOpacity(0.2),
                              offset: const Offset(1.1, 1.1),
                              blurRadius: 10.0),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8, right: 8),
                        child: SingleChildScrollView(
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('Cash'),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 32.0, left: 18, right: 16),
                                  child: ListTile(
                                    title: Text('Cash on Delivery'),
                                    leading: Image.asset(
                                      Assets.paymentType_cash,
                                      height: 50,
                                      width: 50,
                                    ),
                                    onTap: () {
                                      viewModel.setPaymentMethod('cash');
                                      Navigator.pop(context);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ])
              ],
            )));
  }
}
