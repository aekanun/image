import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/viewmodel/ViewModel.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

class MqttModel {
  OrderQueueViewModel _orderQueueViewModel;
  MqttServerClient _client;
  String _osPrefix;
  String topic;
  DtoMqtt _dto;
  BitmapDescriptor riderMarker;
  MqttConnectionState _appConnectionState;

  MqttModel._privateConstructor() {
    _osPrefix = 'Flutter_iOS';
    if (Platform.isAndroid) {
      _osPrefix = 'Flutter_Android';
    }
    this._dto = DtoMqtt();
    this._appConnectionState = MqttConnectionState.disconnected;
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(size: Size(48, 48)), 'assets/img/rider.png')
        .then((onValue) {
      this.riderMarker = onValue;
    });
  }

  static final MqttModel instance = MqttModel._privateConstructor();

  void initializeMQTTClient(
      {OrderQueueViewModel orderQueueViewModel,
      String merchantId,
      String orderId}) {
    this._orderQueueViewModel = orderQueueViewModel;
    this.topic = merchantId + '/' + orderId + '/';

    _client = MqttServerClient(connect_mqtt['broker'], this._osPrefix);
    _client.port = connect_mqtt['port'];
    _client.keepAlivePeriod = 20;
    _client.onDisconnected = onDisconnected;
    _client.secure = false;
    _client.logging(on: true);

    /// Add the successful connection callback, withClientIdentifier =  order_no + pickup_time
    _client.onConnected = onConnected;
    _client.onSubscribed = onSubscribed;
    final MqttConnectMessage connMess = MqttConnectMessage()
        .authenticateAs(connect_mqtt['username'], connect_mqtt['key'])
        .withClientIdentifier(orderId + DateTime.now().toString())
        .withClientIdentifier(this._osPrefix)
        .withWillTopic(
            'willtopic') // If you set this you must set a will message
        .withWillMessage('My Will message')
        .startClean() // Non persistent session for testing
        .withWillQos(MqttQos.atLeastOnce);
    print('Laundry::Mosquitto client connecting....');
    _client.connectionMessage = connMess;

    if (this._appConnectionState == MqttConnectionState.disconnected) {
      connect();
    }
  }

  // Connect to the host
  void connect() async {
    assert(_client != null);
    try {
      print('Laundry::Mosquitto start client connecting....');
      this._appConnectionState = MqttConnectionState.connecting;
      await _client.connect();
    } on Exception catch (e) {
      print('Laundry::client exception - $e');
      disconnect();
    }
  }

  void disconnect() {
    print('Laundry:: Disconnected');
    _client.disconnect();
  }

  void publish(String message) {
    final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
    builder.addString(message);
    _client.publishMessage(
        connect_mqtt['topic'], MqttQos.exactlyOnce, builder.payload);
  }

  /// The subscribed callback
  void onSubscribed(String topic) {
    print('Laundry::Subscription confirmed for topic $topic');
  }

  /// The unsolicited disconnect callback
  void onDisconnected() {
    print('Laundry::OnDisconnected client callback - Client disconnection');
    if (_client.connectionStatus.returnCode ==
        MqttConnectReturnCode.noneSpecified) {
      print('Laundry::OnDisconnected callback is solicited, this is correct');
    }
    this._appConnectionState = MqttConnectionState.disconnected;
  }

  /// The successful connect callback
  /// topic == /wonder/<merchant_id>/<order_id>/tracking/
  void onConnected() {
    this._appConnectionState = MqttConnectionState.connected;
    print('Laundry::Mosquitto client connected....');
    _client.subscribe(connect_mqtt['topic'] + this.topic, MqttQos.atLeastOnce);
    _client.updates.listen((List<MqttReceivedMessage<MqttMessage>> c) {
      final MqttPublishMessage recMess = c[0].payload;
      final String pt =
          MqttPublishPayload.bytesToStringAsString(recMess.payload.message);

      //logic here
      final location = jsonDecode(pt);
      if (location['lat'] != null && location['lon'] != null) {
        LatLng latLng = new LatLng(location['lat'], location['lon']);
        this._dto.markers[this._dto.rMarkerId] =
            Marker(markerId: this._dto.rMarkerId, position: latLng);
        this._orderQueueViewModel.setReceivedMsg(this._dto);
      }
      print(
          'Laundry::Change notification:: topic is <${c[0].topic}>, payload is <-- $pt -->');
      print('');
    });
    print(
        'Laundry::OnConnected client callback - Client connection was sucessful');
  }

  get data {
    LatLng latLng = new LatLng(0, 0);
    this._dto.markers[this._dto.rMarkerId] =
        Marker(markerId: this._dto.rMarkerId, position: latLng);
    return this._dto;
  }
}

class DtoMqtt {
  final markers = <MarkerId, Marker>{};
  final rMarkerId = MarkerId('riderMarkerId');
}
