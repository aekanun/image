import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:laundry_delivery_app/viewmodel/viewModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GoogleMapModel {
  DtoGoogleMap _dto;
  SharedPreferences _prefs;
  Geolocator _geolocator;

  GoogleMapModel._privateConstructor() {
    this._dto = new DtoGoogleMap();
    this._dto.locationController = TextEditingController();
    this._geolocator = new Geolocator();
    SharedPreferences.getInstance().then((value) {
      _prefs = value;
    });
  }
  static final GoogleMapModel instance = GoogleMapModel._privateConstructor();

  Future<void> setOwnLocation(GoogleMapViewModel googleMapViewModel) async {
    googleMapViewModel.setDto(this._dto);
    await _prefs.setDouble("latitude", this._dto.initialposition.latitude);
    await _prefs.setDouble("longitude", this._dto.initialposition.longitude);
  }

  get getOwnLatLng {
    return LatLng(
        _prefs.getDouble("latitude") ?? 13.7517018,
        _prefs.getDouble("longitude") ??
            100.5935206); //default location anudatech
  }

  //default lan en_US null input
  Future<void> getMoveCamera() async {
    print(this._dto.initialposition.latitude.toString());
    print(this._dto.initialposition.longitude.toString());
    List<Placemark> placemark = await this._geolocator.placemarkFromCoordinates(
        this._dto.initialposition.latitude, this._dto.initialposition.longitude,
        localeIdentifier: "th");
    this._dto.locationController.text = placemark[0].subThoroughfare +
        " " +
        placemark[0].thoroughfare +
        " " +
        placemark[0].subAdministrativeArea +
        " " +
        placemark[0].subLocality +
        " " +
        placemark[0].administrativeArea +
        " " +
        placemark[0].postalCode +
        " " +
        placemark[0].country;
  }

  void getUserLocation({GoogleMapViewModel googleMapViewModel}) async {
    if (!(await this._geolocator.isLocationServiceEnabled())) {
      this._dto.activegps = false;
    } else {
      this._dto.activegps = true;

      this._dto.initialposition = LatLng(
          _prefs.getDouble("latitude") ?? 13.7517018,
          _prefs.getDouble("longitude") ?? 100.5935206);

      List<Placemark> placemark = await this
          ._geolocator
          .placemarkFromCoordinates(this._dto.initialposition.latitude,
              this._dto.initialposition.longitude,
              localeIdentifier: "th");
      this._dto.locationController.text = placemark[0].subThoroughfare +
          " " +
          placemark[0].thoroughfare +
          " " +
          placemark[0].subAdministrativeArea +
          " " +
          placemark[0].subLocality +
          " " +
          placemark[0].administrativeArea +
          " " +
          placemark[0].postalCode +
          " " +
          placemark[0].country;
      _addMarker(this._dto.initialposition, placemark[0].name);
      this
          ._dto
          .mapController
          .moveCamera(CameraUpdate.newLatLng(this._dto.initialposition));
    }
    googleMapViewModel.setDto(this._dto);
  }

  Future<void> onGoogleMapCreated(
      GoogleMapController controller, GoogleMapViewModel googleMapViewModel) {
    if (this._dto.mapController == null) {
      this._dto.mapController = controller;
    }
    googleMapViewModel.setDto(this._dto);
  }

  void _addMarker(LatLng location, String address) {
    this._dto.markers.add(Marker(
        markerId: MarkerId(location.toString()),
        position: location,
        infoWindow: InfoWindow(title: address, snippet: "go here"),
        icon: BitmapDescriptor.defaultMarker));
    // googleMapViewModel.setDto(this._dto);
  }

  Future<void> onCameraMove(CameraPosition position) async {
    this._dto.initialposition = position.target;
    // googleMapViewModel.setDto(this._dto);
  }

  Future<String> getCurrentPosition(LatLng latLng) async {
    await _prefs.setDouble("latitude", latLng.latitude);
    await _prefs.setDouble("longitude", latLng.longitude);
    List<Placemark> placemark = await this._geolocator.placemarkFromCoordinates(
        latLng.latitude, latLng.longitude,
        localeIdentifier: "th");
    return this._dto.locationController.text = placemark[0].subThoroughfare +
        " " +
        placemark[0].thoroughfare +
        " " +
        placemark[0].subAdministrativeArea +
        " " +
        placemark[0].subLocality +
        " " +
        placemark[0].administrativeArea +
        " " +
        placemark[0].postalCode +
        " " +
        placemark[0].country;
  }

  get currentAddress {
    return this._dto.locationController.text;
  }
}

class DtoGoogleMap {
  LatLng gpsactual;
  LatLng initialposition = LatLng(0, 0); //thailand
  TextEditingController locationController;
  GoogleMapController mapController;
  Set<Marker> markers = Set();
  bool activegps = true;
}
