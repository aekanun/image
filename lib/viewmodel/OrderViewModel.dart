import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/model/OrderModel.dart';
import 'package:laundry_delivery_app/view/screen/order/Attr.dart';

class OrderViewModel with ChangeNotifier {
  OrderModel _orderModel;
  DtoOrder dto;
  Attr attr = new Attr();
  int selectedRadioWeightPrices = 0;
  int selectedRadioDryTempPrices = 0;
  int selectedRadioWaterTemp = 0;
  bool selectedCheckBoxImmediately = false;

  OrderViewModel();
  OrderViewModel.instance() {
    this._orderModel = OrderModel.instance;
    this.dto = new DtoOrder();
    this.attr.title = "Add order";
    this.attr.scfdRootKey = new GlobalKey<ScaffoldState>();
    this.attr.animeListKey = new GlobalKey<AnimatedListState>();

    //this.dto.store = this._orderModel.getStore;
  }

  void getStore() {
    this.dto.store = this._orderModel.getStore;
    //notifyListeners();
  }

  Future<void> confirmOrder() async {
    this._orderModel.confirmOrder(this);
  }

  get getSelectedCheckBoxImmediately {
    return this.selectedCheckBoxImmediately;
  }

  void updateSelectedCheckBoxImmediately(bool value) {
    print('selectedCheckBox: $value');
    this.selectedCheckBoxImmediately = value;
    if (this.selectedCheckBoxImmediately) {
      setDateTimeOrder(DateTime.now());
    }
    notifyListeners();
  }

  get getSelectedRadioWaterTemp {
    return this.selectedRadioWaterTemp;
  }

  void updateSelectedRadioWaterTemp(int selectedRadio) {
    print('selectWaterTemp: $selectedRadio');
    this.selectedRadioWaterTemp = selectedRadio;
    notifyListeners();
  }

  get getSelectedRadioWeightPrices {
    return this.selectedRadioWeightPrices;
  }

  void updateSelectedRadioWeightPrices(int selectedRadio) {
    print('selectWeightPrices: $selectedRadio');
    this.selectedRadioWeightPrices = selectedRadio;
    notifyListeners();
  }

  get getSelectedRadioDryTempPrices {
    return this.selectedRadioDryTempPrices;
  }

  void updateSelectedRadioDryTempPrices(int selectedRadio) {
    print('selectDry: $selectedRadio');
    this.selectedRadioDryTempPrices = selectedRadio;
    notifyListeners();
  }

  void setDto(DtoOrder dto) {
    this.selectedRadioWeightPrices = 0;
    this.selectedRadioDryTempPrices = 0;
    this.selectedRadioWaterTemp = 0;
    this.dto = dto;
    notifyListeners();
  }

  void clearOrder() {
    this._orderModel.clearOrder();
  }

  void clearOrderSubmit() {
    this._orderModel.clearOrderSubmit();
  }

  void removeItem(int basketNo, int charge) {
    this._orderModel.removeItemByIndex(this, basketNo, charge);
    notifyListeners();
  }

  void setPaymentMethod(String paymentMethod) {
    this._orderModel.setPaymentMethod(this, paymentMethod);
  }

  void setDateTimeOrder(DateTime dateTime) {
    this._orderModel.setDateTime(this, dateTime);
  }

  void setOnConfirmOrder(TextEditingController noteController) {
    this._orderModel.setOrderList(
        this,
        this.selectedRadioWeightPrices,
        this.selectedRadioWaterTemp,
        this.selectedRadioDryTempPrices,
        noteController.text);
  }
}
