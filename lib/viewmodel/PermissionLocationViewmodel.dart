import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:laundry_delivery_app/model/PermisstionLocationModel.dart';
import 'package:laundry_delivery_app/model/model.dart';

class PermissionLocationViewmodel with ChangeNotifier {
  PermisstionLocationModel _permisstionLocationModel;
  LaundryStoreModel _laundryStoreModel;
  GoogleMapModel _googleMapModel;
  DtoPermissionLocation dto;
  String currentNavigation;

  PermissionLocationViewmodel();
  PermissionLocationViewmodel.instance() {
    this._permisstionLocationModel = PermisstionLocationModel.instance;
    this._laundryStoreModel = LaundryStoreModel.instance;
    this._googleMapModel = GoogleMapModel.instance;
    this.dto = new DtoPermissionLocation();
    this._permisstionLocationModel.requestPermission(this);
  }

  void requestPermission() {
    this._permisstionLocationModel.requestPermission(this);
  }

  setDto(DtoPermissionLocation dto) async {
    this.dto = dto;
    this.dto.messageLocation = await this._googleMapModel.getCurrentPosition(
        new LatLng(
            this.dto.locationData.latitude, this.dto.locationData.longitude));

    this._laundryStoreModel.setCurrentLocation(
        currentAddress: this.dto.messageLocation,
        locationData: this.dto.locationData);
    print('lat: ' + this.dto.locationData.latitude.toString());
    print('lon: ' + this.dto.locationData.longitude.toString());
    print('errorMessage: =========' + this.dto.errorMessage);
    print('messageLocation: =========' + this.dto.messageLocation);
    notifyListeners();
  }

  // Widget get getNavigation {
  //   if (currentNavigation == u08) {
  //     return LaundryHomeStoreScreen();
  //   } else {
  //     if (this._laundryStoreModel.dto.lattitude != 0 &&
  //         this._laundryStoreModel.dto.longitude != 0) {
  //       return LaundryHomeStoreScreen();
  //     }
  //     return FindLocationScreen();
  //   }
  // }

  // void updateNavigation(String navigation) {
  //   currentNavigation = navigation;
  //   notifyListeners();
  // }
}
