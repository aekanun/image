import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/utility/ScaleRoute.dart';
import 'package:laundry_delivery_app/view/styles/ColorsConst.dart';
import 'package:laundry_delivery_app/viewmodel/OrderQueueViewModel.dart';
import 'package:provider/provider.dart';

import 'U14_OrderTrackingLocationScreen.dart';

class OrderQueueScreen extends StatefulWidget {
  OrderQueueState createState() => OrderQueueState();
}

class OrderQueueState extends State<OrderQueueScreen> {
  @override
  void initState() {
    super.initState();
    OrderQueueViewModel orderQueue =
        Provider.of<OrderQueueViewModel>(context, listen: false);
    orderQueue.getOrderQueue();
    print(
        '==============================OrderQueue init==============================');
  }

  @override
  void dispose() {
    print(
        '==============================OrderQueue dispose==============================');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<OrderQueueViewModel>(builder: (context, orderQueue, _) {
      return Scaffold(
        backgroundColor: ColorsConst.white,
        appBar: AppBar(
          key: orderQueue.dto.scfdRootKey,
          title: Text(
            orderQueue.dto.title,
            style: TextStyle(color: Colors.black),
          ),
          bottomOpacity: 0.0,
          elevation: 0.0,
          backgroundColor: ColorsConst.white,
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              color: Colors.black,
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              child: orderQueue.dto.orderQueue == null ||
                      orderQueue.dto.orderQueue.data.length == 0
                  ? Center(child: Text('ไม่มีข้อมูล'))
                  : ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: orderQueue.dto.orderQueue.data.length,
                      itemBuilder: (context, index) {
                        return Column(
                          children: <Widget>[
                            Container(
                              child: ListTile(
                                leading: Text((index + 1).toString()),
                                title: Text(orderQueue
                                    .dto.orderQueue.data[index].pickupTime),
                                subtitle: Text('order: ' +
                                    orderQueue
                                        .dto.orderQueue.data[index].orderNo
                                        .toString()),
                                trailing: Text('฿ ' +
                                    orderQueue
                                        .dto.orderQueue.data[index].totalCharge
                                        .toString()),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      ScaleRoute(
                                          page: OrderTrackingLocationScreen(
                                              index: index)));
                                },
                              ),
                            )
                          ],
                        );
                      }),
            ),
            InkWell(
              child: Container(
                alignment: Alignment.center,
                height: 48.0,
                child: Text("กลับ",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18, color: Colors.purple)),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      );
    });
  }
}
