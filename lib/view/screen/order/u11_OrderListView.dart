import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/view/styles/AppTheme.dart';
import 'package:laundry_delivery_app/view/styles/ColorsConst.dart';
import 'package:laundry_delivery_app/viewmodel/OrderViewModel.dart';
import 'package:provider/provider.dart';

class OrderListView extends StatelessWidget {
  final ScrollController _scrollController = ScrollController();

  void _scrollToSelectedContent(
      bool isExpanded, double previousOffset, int index, GlobalKey myKey) {
    final keyContext = myKey.currentContext;

    if (keyContext != null) {
      // make sure that your widget is visible
      final box = keyContext.findRenderObject() as RenderBox;
      //เกิด error จุดนี้  ScrollController not attached to any scroll views.
      _scrollController.animateTo(
          isExpanded ? (box.size.height * index) : previousOffset,
          duration: Duration(milliseconds: 500),
          curve: Curves.linear);
    }
  }

  ExpansionTile _buildExpansionTile(OrderViewModel order, int index) {
    final GlobalKey expansionTileKey = GlobalKey();
    double previousOffset;

    int charge;

    charge = order.dto.order.orderDetail[index].waterTempPrices.price +
        order.dto.order.orderDetail[index].dryTempPrices.price +
        order.dto.order.orderDetail[index].weightPrices.price;
    return ExpansionTile(
      key: expansionTileKey,
      onExpansionChanged: (isExpanded) {
        if (isExpanded) previousOffset = _scrollController.offset;
        _scrollToSelectedContent(
            isExpanded, previousOffset, index, expansionTileKey);
      },
      title: Text('Basket ' + (index + 1).toString()),
      children: <Widget>[
        new ListTile(
          //leading: Icon(Icons.shopping_basket),
          title: Text('Clothes type: '),
          trailing: Text('Regular'),
        ),
        new ListTile(
          title: Text('Clothes weight: ' +
              order.dto.order.orderDetail[index].weightPrices.weight
                  .toString() +
              " kg"),
          trailing: Text(
              order.dto.order.orderDetail[index].weightPrices.price.toString() +
                  " ฿"),
        ),
        new ListTile(
          title: Text('Water temperature: ' +
              order.dto.order.orderDetail[index].waterTempPrices.temp),
          trailing: Text(order
                  .dto.order.orderDetail[index].waterTempPrices.price
                  .toString() +
              " ฿"),
        ),
        new ListTile(
          title: Text('Dryer temperature: ' +
              order.dto.order.orderDetail[index].dryTempPrices.temp),
          trailing: Text(order.dto.order.orderDetail[index].dryTempPrices.price
                  .toString() +
              " ฿"),
        ),
        new ListTile(
          title: Text('Charge: '),
          trailing: Text(charge.toString() + " ฿"),
        ),
        new ListTile(
          trailing: IconButton(
            icon: Icon(Icons.delete),
            color: Colors.red,
            onPressed: () {
              order.removeItem(
                  order.dto.order.orderDetail[index].basketNo, charge);
            },
          ),
          // onTap: () {
          //   viewModel.removeItem(viewModel.order.orderDetail[index].basketNo);
          // },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<OrderViewModel>(builder: (context, order, _) {
      return Scaffold(
        backgroundColor: ColorsConst.white,
        appBar: AppBar(
          //  key: attr.scfdRootKey,
          title: Text('Order List'),
          bottomOpacity: 0.0,
          elevation: 0.0,
          backgroundColor: ColorsConst.white,
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              color: Colors.black,
              onPressed: () {
                order.clearOrder();
                Navigator.pushNamed(context, u08);
              }),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add),
              color: Colors.black,
              onPressed: () {
                //  order.updateNavigation("order_detail");
                Navigator.pushNamed(context, u10);
              },
            ),
          ],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              child: order.dto.order == null ||
                      order.dto.order.orderDetail.length == 0
                  ? Center(child: Text('Empty'))
                  : ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: order.dto.order.orderDetail.length,
                      itemBuilder: (context, index) =>
                          _buildExpansionTile(order, index),
                    ),
            ),
            InkWell(
              onTap: () {
                //  order.updateNavigation(u12);

                if (order.dto.order.orderDetail != null &&
                    order.dto.order.orderDetail.length > 0) {
                  Navigator.pushNamed(context, u12);
                } else {
                  print('cannot route to confirms');
                }
              },
              child: Container(
                padding: new EdgeInsets.all(8.0),
                color: AppTheme.nearyPurple.withOpacity(0.5),
                height: 48.0,
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.shopping_cart),
                          Text(order.dto.order != null
                              ? order.dto.order.orderDetail.length.toString() +
                                  " items | " +
                                  order.dto.order.totalChange.toString() +
                                  " ฿"
                              : ""),
                        ],
                      ),
                    ),
                    Spacer(),
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      //margin: EdgeInsets.all(10),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(30.0),
                        child: Container(
                          color: Colors.white,
                          width: 80,
                          height: 40,
                          child: Center(
                            child: Text('View Cart'),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      );
    });
  }
}
