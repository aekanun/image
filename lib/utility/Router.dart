import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/view/screen/U01_IndexScreen.dart';
import 'package:laundry_delivery_app/view/screen/laundry_home/FindLocationScreen.dart';
import 'package:laundry_delivery_app/view/screen/order/u11_OrderListView.dart';
import 'package:laundry_delivery_app/view/screen/signin/U02_SignInScreen.dart';
import 'package:laundry_delivery_app/view/screen/signin/U02_U03_SignInHomeScreen.dart';
import 'package:laundry_delivery_app/view/screen/register/U04_U07_RegisterHomeScreen.dart';
import 'package:laundry_delivery_app/view/screen/laundry_home/U08_LaundryHomeStoreScreen.dart';
import 'package:laundry_delivery_app/view/screen/laundry_home/U09_PickLocationScreen.dart';
import 'package:laundry_delivery_app/view/screen/order/U10_OrderListScreen.dart';
import 'package:laundry_delivery_app/view/screen/order/U12_ConfirmOrderListScreen.dart';
import 'package:laundry_delivery_app/view/screen/order/U13_OrderRequestDriverScreen.dart';
import 'package:laundry_delivery_app/view/screen/U16_SettingScreen.dart';
import 'package:laundry_delivery_app/view/screen/U17_OrderHistoryScreen.dart';
import 'package:laundry_delivery_app/view/screen/U18_EditPhoneNumberScreen.dart';
import 'package:laundry_delivery_app/view/screen/U19_OtpVerificationCodeForEditScreen.dart';
import 'package:laundry_delivery_app/view/screen/U20_VideoTutorialScreen.dart';
import 'package:laundry_delivery_app/view/screen/U21_EditProfileScreen.dart';
import 'package:laundry_delivery_app/view/screen/order_queue/U22_OrderQueueScreen.dart';
import 'package:laundry_delivery_app/view/screen/register/U05_VerifyPhoneNumberScreen.dart';
import 'package:laundry_delivery_app/view/screen/register/U07_IntroScreen.dart';
import 'package:laundry_delivery_app/view/screen/signin/U03_UserSignInScreen.dart';

class Routers {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => IndexScreen());
        break;
      case '/sign_in_home':
        return MaterialPageRoute(builder: (_) => SignInHomeScreen());
        break;
      case '/signin':
        return MaterialPageRoute(builder: (_) => SignInScreen()); //U02
        break;
      case '/user_sign_in':
        return MaterialPageRoute(builder: (_) => UserSignInScreen()); //U03
        break;
      case '/user_register':
        return MaterialPageRoute(
            builder: (_) => RegisterHomeScreen()); //U04-U07
        break;
      case '/verify_phone':
        return MaterialPageRoute(
            builder: (_) => VerifyPhoneNumberScreen()); //U05
        break;
      // case '/otp_verification':
      //   return MaterialPageRoute(
      //       builder: (_) => OtpVerificationCodeScreen()); //U06
      //   break;
      case '/intro_welcome':
        return MaterialPageRoute(builder: (_) => IntroScreen()); //U07
        break;
      case '/find_location':
        return MaterialPageRoute(
          builder: (_) => FindLocationScreen(),
        );
        break;
      case '/laundry_home':
        return MaterialPageRoute(
            builder: (_) => LaundryHomeStoreScreen()); //U08
        break;
      case '/pick_location':
        return MaterialPageRoute(builder: (_) => PickLocationScreen()); //U09
        break;
      case '/order_list':
        return MaterialPageRoute(builder: (_) => OrderListScreen()); //U10
        break;
      case '/order_list_view':
        return MaterialPageRoute(builder: (_) => OrderListView()); //U11
        break;
      case '/order_detail':
        //  return MaterialPageRoute(builder: (_) => OrderDetail()); //U10
        break;
      case '/confirm_order':
        return MaterialPageRoute(
            builder: (_) => ConfirmOrderListScreen()); //U12
        break;
      case '/order_request':
        return MaterialPageRoute(
            builder: (_) => OrderRequestDriverScreen()); //U13
        break;
      // case '/order_tracking_location':
      //   return MaterialPageRoute(
      //       builder: (_) => OrderTrackingLocationMapScreen()); //U14
      //   break;
      // case '/order_tracking_timeline':
      //   return MaterialPageRoute(
      //       builder: (_) => OrderTrackingTimeLineScreen()); //U15
      //   break;
      case '/setting':
        return MaterialPageRoute(builder: (_) => SettingScreen()); //U16
        break;
      case '/order_history':
        return MaterialPageRoute(builder: (_) => OrderHistoryScreen()); //U17
        break;
      case '/edit_phone_number':
        return MaterialPageRoute(builder: (_) => EditPhoneNumberScreen()); //U18
        break;
      case '/otp_verification_for_edit':
        return MaterialPageRoute(
            builder: (_) => OtpVerificationCodeForEditScreen()); //U19
        break;
      case '/video_tutorial':
        return MaterialPageRoute(builder: (_) => VideoTutorialScreen()); //U20
        break;
      case '/edit_profile':
        return MaterialPageRoute(builder: (_) => EditProfileScreen()); //U21
        break;
      case '/order_queue':
        return MaterialPageRoute(builder: (_) => OrderQueueScreen()); //U22
        break;
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}
