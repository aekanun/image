import 'package:laundry_delivery_app/data/entity/DryTempPrice.dart';
import 'package:laundry_delivery_app/data/entity/WaterTempPrice.dart';
import 'package:laundry_delivery_app/data/entity/WeightPrice.dart';

class OrderDetail {
  int basketNo;
  WeightPrice weightPrice;
  WaterTempPrice waterTempPrice;
  DryTempPrice dryTempPrice;
  String note;

  OrderDetail(
      {this.basketNo,
      this.weightPrice,
      this.waterTempPrice,
      this.dryTempPrice,
      this.note});

  OrderDetail.fromJson(Map<String, dynamic> json) {
    basketNo = json['basket_no'];
    weightPrice = json['weight_price'] != null
        ? new WeightPrice.fromJson(json['weight_price'])
        : null;
    waterTempPrice = json['water_temp_price'] != null
        ? new WaterTempPrice.fromJson(json['water_temp_price'])
        : null;
    dryTempPrice = json['dry_temp_price'] != null
        ? new DryTempPrice.fromJson(json['dry_temp_price'])
        : null;
    note = json['note'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['basket_no'] = this.basketNo;
    if (this.weightPrice != null) {
      data['weight_price'] = this.weightPrice.toJson();
    }
    if (this.waterTempPrice != null) {
      data['water_temp_price'] = this.waterTempPrice.toJson();
    }
    if (this.dryTempPrice != null) {
      data['dry_temp_price'] = this.dryTempPrice.toJson();
    }
    data['note'] = this.note;
    return data;
  }
}
