import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:laundry_delivery_app/data/entity/DryTempPrice.dart';
import 'package:laundry_delivery_app/data/entity/Order.dart';
import 'package:laundry_delivery_app/data/entity/WaterTempPrice.dart';
import 'package:laundry_delivery_app/data/entity/WeightPrice.dart';
import 'package:laundry_delivery_app/data/entity/order/CustomerLocation.dart';
import 'package:laundry_delivery_app/data/entity/order/OrderDetail.dart';
import 'package:laundry_delivery_app/data/entity/order/OrderSubmit.dart';
import 'package:laundry_delivery_app/data/entity/store/Store.dart';
import 'package:laundry_delivery_app/model/AuthenModel.dart';
import 'package:laundry_delivery_app/model/GoogleMapModel.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/viewmodel/ViewModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'LaundryStoreModel.dart';
import 'services/service.dart';

class OrderModel {
  LaundryStoreModel _laundryStoreModel;
  GoogleMapModel _googleMapModel;
  AuthenModel _authenModel;

  Service _service;
  DtoOrder _dto;
  String _accessToken;
  int totalChange = 0;

  OrderModel._privateConstructor() {
    this._dto = new DtoOrder();
    this._laundryStoreModel = LaundryStoreModel.instance;
    this._googleMapModel = GoogleMapModel.instance;
    this._authenModel = AuthenModel.instance;
    this._service = new Service();

    this._dto.order = new Order();
    this._dto.order.orderDetail = new List<OrderDetail>();
    // this._dto.store = _laundryStoreModel.dto.store;

    SharedPreferences.getInstance().then((value) {
      this._accessToken = value.getString('accessToken') ?? '';
    });
  }
  static final OrderModel instance = OrderModel._privateConstructor();

  get getStore {
    this._dto.store = this._laundryStoreModel.dto.store;
    return this._dto.store;
  }

  setOrderList(OrderViewModel orderViewModel, int selectedRadioWeightPrices,
      int selectedRadioWaterTemp, int selectedRadioDryTempPrices, String text) {
    /* customer address from google map */
    this._dto.order.customerAddress = this._googleMapModel.currentAddress;
    print('=============order customer address==============');
    print(this._dto.order.customerAddress);
    /* merchant_id from laundry store */
    this._dto.order.merchantId = this._laundryStoreModel.dto.store.merchantId;
    // /* initial pick time */
    this._dto.order.pickupTime = "";
    /* customer location from google map */
    CustomerLocation customerLocation = new CustomerLocation();
    LatLng latLng = this._googleMapModel.getOwnLatLng;
    customerLocation.lat = latLng.latitude;
    customerLocation.long = latLng.longitude;
    this._dto.order.customerLocation =
        customerLocationFromJson(customerLocationToJson(customerLocation));
    if (this._dto.order.orderDetail.length == 3) {
      this._dto.errorMessage = "Fail";
    } else {
      this._dto.errorMessage = "Success";
      OrderDetail data = new OrderDetail();
      data.basketNo = this._dto.order.orderDetail.length;
      /* weight price */
      // data.weightPrices =
      //     this._dto.store.weightPrices[selectedRadioWeightPrices];
      // data.weightPrices = WeightPrice.fromJson(json)

      data.weightPrices = WeightPrice.fromJson(
          this._dto.store.weightPrices[selectedRadioWeightPrices].toJson());
      totalChange += data.weightPrices.price;
      print('========before=====');
      print(data.weightPrices.toJson());
      print('========after=====');
      // /* water temp price */
      data.waterTempPrices = WaterTempPrice.fromJson(
          this._dto.store.waterTempPrices[selectedRadioWaterTemp].toJson());
      totalChange += data.waterTempPrices.price;
      print('========before=====');
      print(data.waterTempPrices.toJson());
      print('========after=====');
      // /* dry temp price */
      data.dryTempPrices = DryTempPrice.fromJson(
          this._dto.store.dryTempPrices[selectedRadioDryTempPrices].toJson());
      totalChange += data.dryTempPrices.price;
      print('========before=====');
      print(data.dryTempPrices.toJson());
      print('========after=====');
      // /* add order detail */

      data.note = text;
      // this
      //     ._dto
      //     .order
      //     .orderDetail
      //     .add(orderDetailFromJson(orderDetailToJson(data)));
      this._dto.order.orderDetail.add(data);
    }
    this._dto.order.totalChange = totalChange;
    this._dto.order.paymentOption = "cash";
    //  this._dto.order = orderFromJson(orderToJson(this._dto.order));
    orderViewModel.setDto(this._dto);
  }

  void removeItemByIndex(
      OrderViewModel orderViewModel, int basketNo, int charge) {
    print(basketNo.toString());
    print('remove charge: ' + charge.toString());
    this.totalChange -= charge;
    this._dto.order.totalChange -= charge;
    this
        ._dto
        .order
        .orderDetail
        .removeWhere((element) => element.basketNo == basketNo);
    orderViewModel.setDto(this._dto);
  }

  setDateTime(OrderViewModel orderViewModel, DateTime dateTime) {
    /* pick time */
    this._dto.order.pickupTime = dateTime.toString();
    this._dto.errorMessage = "update datetime";
    orderViewModel.setDto(this._dto);
  }

  setPaymentMethod(OrderViewModel orderViewModel, String paymentMethod) {
    this._dto.order.paymentOption = paymentMethod;
    this._dto.errorMessage = "update payment";
    orderViewModel.setDto(this._dto);
  }

  Future<void> confirmOrder(OrderViewModel orderViewModel) async {
    this._dto.orderSubmit = OrderSubmit.fromJson(await _service.post(
        url_order_submit, orderToJson(this._dto.order), this._accessToken));
    if (this._dto.orderSubmit != null) {
      if (_dto.orderSubmit.code == 0) {
        this._dto.errorMessage = "submit order success";
        clearOrder();
        orderViewModel.setDto(this._dto);
      } else {
        this._dto.errorMessage = "submit order fail";
        clearOrder();
        orderViewModel.setDto(this._dto);
      }
    }
  }

  void clearOrder() {
    this._dto.order.orderDetail.clear();
  }

  void clearOrderSubmit() {
    this._dto.orderSubmit.clear();
  }
}

class DtoOrder {
  String errorMessage;
  Store store;
  Order order;
  OrderSubmit orderSubmit;
  DtoOrder();
}
