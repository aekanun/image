import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/utility/ScaleRoute.dart';
import 'package:laundry_delivery_app/view/styles/AppTheme.dart';
import 'package:laundry_delivery_app/view/widget/BaseAppBar.dart';
import 'package:laundry_delivery_app/view/widget/DialogUtil.dart';
import 'package:laundry_delivery_app/viewmodel/LaundryStoreViewModel.dart';
import 'package:provider/provider.dart';

import 'LaundryStoreDetail.dart';

class LaundryHomeStoreScreen extends StatefulWidget {
  LaundryHomeStoreState createState() => LaundryHomeStoreState();
}

class LaundryHomeStoreState extends State<LaundryHomeStoreScreen> {
  @override
  void initState() {
    super.initState();
    // LaundryStoreViewModel laundryStoreViewModel =
    //     Provider.of<LaundryStoreViewModel>(context, listen: false);
    // laundryStoreViewModel.getListStore();
    print(
        '==============================LaundryHome init==============================');
  }

  @override
  void dispose() {
    print(
        '==============================LaundryHome dispose==============================');
    super.dispose();
  }

  Future<bool> onWillPop() {
    showCupertinoDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: Text("ออกจากระบบ"),
            content: Text("\nขอบคุณที่ใช้บริการ 💙"),
            actions: <Widget>[
              CupertinoDialogAction(
                isDefaultAction: true,
                child: Text("ตกลง"),
                onPressed: () =>
                    SystemChannels.platform.invokeMethod('SystemNavigator.pop'),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        // key: keyScaffold,
        appBar: BaseToolBar(
          title: 'เลือกร้าน',
          leftIcon: Icons.home,
          onLeftPress: () {
            Navigator.pushNamed(context, u22);
          },
          rightIcon: Icons.person,
          onRightPress: () {
            Navigator.pushNamed(context, u16);
          },
        ).build(context),
        backgroundColor: Colors.white,
        body: ChangeNotifierProvider(
          create: (context) => LaundryStoreViewModel.instance(),
          child: Consumer<LaundryStoreViewModel>(
            builder: (context, laundry, child) {
              print(
                  '==================== consumer laundry ====================');
              return Stack(
                children: <Widget>[
                  Container(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: AppBar().preferredSize.height,
                            // color: Colors.green[800],
                            child: InkWell(
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    // LayoutBuilder(
                                    //     builder: (context, constraint) {
                                    //   return new Image(
                                    //       image: new AssetImage(
                                    //           "assets/img/location_map.png"),
                                    //       width: constraint.biggest.height / 2,
                                    //       height: constraint.biggest.height / 2,
                                    //       fit: BoxFit.scaleDown,
                                    //       alignment: FractionalOffset.center);
                                    // }),
                                    Icon(
                                      Icons.location_on_outlined,
                                      color: AppTheme.nearyPurple,
                                    ),
                                    Flexible(
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                            laundry.dto.currentAddress != null
                                                ? "จัดส่งที่: " +
                                                    laundry.dto.currentAddress
                                                : "จัดส่งที่: ",
                                            style:
                                                TextStyle(color: Colors.black),
                                            strutStyle:
                                                StrutStyle(fontSize: 14.0),
                                            overflow: TextOverflow.ellipsis),
                                      ),
                                    ),
                                    Icon(Icons.arrow_forward_ios),
                                    // LayoutBuilder(
                                    //     builder: (context, constraint) {
                                    //   return new Image(
                                    //       image: new AssetImage(
                                    //           "assets/img/edit.png"),
                                    //       width: constraint.biggest.height / 2,
                                    //       height: constraint.biggest.height / 2,
                                    //       fit: BoxFit.scaleDown,
                                    //       alignment: FractionalOffset.center);
                                    // }),
                                  ],
                                ),
                              ),
                              onTap: () async {
                                final statusLocation =
                                    await Navigator.pushNamed(context, u09);
                                if (statusLocation) {
                                  laundry.updateData();
                                }
                              },
                            ),
                          ),
                          SizedBox(
                            height: 200.0,
                            width: 350.0,
                            child: CarouselSlider.builder(
                                itemCount: laundry.dto.storeResponse != null
                                    ? laundry.dto.storeResponse.advertise.length
                                    : 0,
                                itemBuilder: (context, index, _) {
                                  if (laundry.dto.storeResponse != null &&
                                      laundry.dto.storeResponse.advertise
                                              .length !=
                                          0) {
                                    return Container(
                                      child: Image.network(laundry
                                          .dto.storeResponse.advertise[index]),
                                    );
                                  }
                                },
                                options: CarouselOptions(
                                  height: 400,
                                  aspectRatio: 16 / 9,
                                  viewportFraction: 0.8,
                                  initialPage: 0,
                                  enableInfiniteScroll: true,
                                  reverse: false,
                                  autoPlay: true,
                                  autoPlayInterval: Duration(seconds: 3),
                                  autoPlayAnimationDuration:
                                      Duration(milliseconds: 800),
                                  autoPlayCurve: Curves.fastOutSlowIn,
                                  enlargeCenterPage: true,
                                  // onPageChanged: callbackFunction,
                                  scrollDirection: Axis.horizontal,
                                )),
                          ),
                          Padding(
                            padding: EdgeInsets.all(10),
                            child: ListView.builder(
                                // padding: EdgeInsets.only(bottom: 8.0),
                                controller: laundry.controller,
                                // physics: const AlwaysScrollableScrollPhysics(),
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: laundry.dto.storeResponse != null
                                    ? laundry.dto.storeResponse.store.length
                                    : 0,
                                itemBuilder: (context, index) {
                                  if (laundry.dto.storeResponse != null &&
                                      laundry.dto.storeResponse.store.length !=
                                          0) {
                                    final data =
                                        laundry.dto.storeResponse.store[index];
                                    return Container(
                                      padding: EdgeInsets.only(left: 12.0),
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        color: Colors.deepPurple,
                                      ),
                                      height: 80,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(10.0),
                                              bottomRight:
                                                  Radius.circular(10.0)),
                                          color: Colors.deepPurple[50],
                                        ),
                                        child: ListTile(
                                          contentPadding: EdgeInsets.only(
                                              left: 32, right: 32, bottom: 8),
                                          title: Text(
                                            data.name,
                                            style: TextStyle(
                                                color: Colors.black87),
                                          ),
                                          subtitle: Text(
                                            data.address,
                                            style: TextStyle(
                                                color: Colors.blueGrey[400]),
                                          ),
                                          trailing: Icon(
                                            Icons.check_circle,
                                            color: Colors.greenAccent,
                                          ),
                                          onTap: () async {
                                            print(data.name);
                                            final statusStore =
                                                await Navigator.push(
                                                    context,
                                                    ScaleRoute(
                                                        page:
                                                            LaundryStoreDetail(
                                                                store: data,
                                                                viewModel:
                                                                    laundry)));
                                            if (statusStore != null) {
                                              print(
                                                  "error store: $statusStore");
                                              if (statusStore) {
                                                //act.onSuccess(data);
                                                laundry.setStoreIndex(data);
                                                Navigator.pushReplacementNamed(
                                                    context, u10);
                                              } else {
                                                DialogUtil()
                                                    .showAlertDialogLaundryStore(
                                                        context,
                                                        "เลือกร้านใหม่",
                                                        "ไม่สามารถทำรายการได้",
                                                        "เนื่องจากเกินระยะเวลาทำการของร้าน");
                                              }
                                            }
                                          },
                                        ),
                                      ),
                                      margin: EdgeInsets.only(
                                          bottom: 8, left: 16, right: 16),
                                    );
                                  }
                                }),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
      onWillPop: onWillPop,
    );
  }
}
