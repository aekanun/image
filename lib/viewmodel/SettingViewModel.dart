import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/model/AuthenModel.dart';
import 'package:laundry_delivery_app/model/SettingModel.dart';

class SettingViewModel extends ChangeNotifier {
  AuthenModel _authenModel;
  SettingModel _settingModel;
  DtoSetting dto;

  SettingViewModel();
  SettingViewModel.instance() {
    this._authenModel = AuthenModel.instance;
    this._settingModel = SettingModel.instance;
    this.dto = new DtoSetting();
    this._settingModel.getDataFromDb(this);
  }

  void logout() {
    this._authenModel.logout();
  }

  void setProfile(DtoSetting dto) {
    this.dto = dto;
    notifyListeners();
  }
}
