import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/model/RegisterModel.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import '../utility/enums.dart';

class RegisterViewModel with ChangeNotifier {
  DtoRegister dto;
  String currentNavigation = "";
  LoadingStatus loadingStatus = LoadingStatus.none;

  RegisterModel _registerModel;

  RegisterViewModel();
  RegisterViewModel.instance() {
    _registerModel = RegisterModel.instance;
    this.dto = new DtoRegister();
  }

  // Future<void> loginWithFacebook() async {
  //   this.loadingStatus = LoadingStatus.facebook;
  //   await _registerModel.loginWithFacebook(this);
  // }

  // Future<void> loginWithGmail() async {
  //   this.loadingStatus = LoadingStatus.google;
  //   await _registerModel.loginWithGmail(this);
  // }

  setRegisterData(Map mapController) async {
    this.loadingStatus = LoadingStatus.register;
    await _registerModel.setRegisterData(mapController, this);
  }

  setOtpData(Map mapController) async {
    this.loadingStatus = LoadingStatus.otp;
    await _registerModel.setOtp(mapController, this);
  }

  setDto(DtoRegister dto) {
    this.dto = dto;
    switch (this.loadingStatus) {
      case LoadingStatus.none:
        //to do
        break;
      case LoadingStatus.register:
        if (this.dto.dataEntry) {
          if (this.dto.errorMessage == "register success") {
            print(
                '========================customer register success=========================');
            this._registerModel.updateNavigation(u06, this);
          } else {
            print("Please try again!!");
          }
        }
        break;
      case LoadingStatus.otp:
        if (this.dto.otp != "") {
          if (this.dto.errorMessage == "verify success") {
            print(
                '========================otp register success=========================');
            this._registerModel.updateNavigation(u07, this);
          }
        } else {
          print("registerViewModel otp == null");
        }
        break;
      case LoadingStatus.social:
        if (this.dto.dataEntry) {
          if (this.dto.errorMessage == "register success") {
            print(
                '========================social register success=========================');
            this._registerModel.updateNavigation(u06, this);
          } else {
            print("Please try again!!");
          }
        }
        break;
    }
    // notifyListeners();
  }

  void updateNavigation(DtoRegister dto) {
    print(
        '===========================================register update navigation');
    this.dto.currentNavigation = dto.currentNavigation;
    notifyListeners();
  }

  void setDefaultCurrentnavigation() {
    this._registerModel.setDefaultCurrentnavigation();
  }

  void setMobilePhoneNumber(String phoneNumber) {
    this.loadingStatus = LoadingStatus.social;
    this
        ._registerModel
        .socialRegister(registerViewModel: this, phoneNumber: phoneNumber);
    //this._registerModel.setMobilePhoneNumber(phoneNumber);
    //this._registerModel.updateNavigation(u06, this);
  }
}
