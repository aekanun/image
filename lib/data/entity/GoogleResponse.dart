import 'package:google_sign_in/google_sign_in.dart';

class GoogleResponse {
  GoogleSignIn googleSignIn;
  GoogleSignInAuthentication googleKey;

  // print(googleKey.accessToken);
  // print(googleKey.idToken);
  // print(googleSignIn.currentUser.id);
  // print(googleSignIn.currentUser.displayName);
  // print(googleSignIn.currentUser.email);
  // print(googleSignIn.currentUser.photoUrl);

  GoogleResponse(this.googleSignIn, this.googleKey);

  factory GoogleResponse.fromMap(
      GoogleSignIn googleSignIn, GoogleSignInAuthentication googleKey) {
    if (googleSignIn == null && googleKey == null) return null;
    return GoogleResponse(googleSignIn, googleKey);
  }
}
