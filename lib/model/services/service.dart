import 'dart:convert';
import 'dart:io';
import 'package:laundry_delivery_app/model/apis/AppException.dart';
import 'package:meta/meta.dart';

import 'package:http/http.dart' as http;

class Service {
  // final String _baseUrl = "https://wonderwash.free.beeceptor.com";
  //final String _baseUrl = "https://kirigaya.free.beeceptor.com";
  final String _baseUrl = "http://192.168.24.90:8111";
  final String _base =
      "https://run.mocky.io/v3/d5ae9c12-753a-4c4d-9ae8-b23ff14d328b";

  Future<dynamic> getGraphResponse(String url) async {
    dynamic responseJson;
    try {
      responseJson = await http.get(Uri.parse(url));
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  Future<dynamic> get(String url, String token) async {
    dynamic responseJson;
    try {
      final response = await http.get(
        Uri.parse(_baseUrl + url),
        headers: <String, String>{
          'content-type': 'application/json; charset=utf-8',
          'Authorization': 'Bearer $token',
        },
      );
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  Future<dynamic> post(String url, dynamic body, String token) async {
    dynamic responseJson;
    try {
      final response = await http.post(
        Uri.parse(_baseUrl + url),
        headers: <String, String>{
          'content-type': 'application/json; charset=utf-8',
          'Authorization': 'Bearer $token',
        },
        body: body,
      );
      responseJson = returnResponse(response);
    } on Exception {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  Future<dynamic> postMock(String url, dynamic body, String token) async {
    dynamic responseJson;
    try {
      final response = await http.post(
        Uri.parse(_base + url),
        headers: <String, String>{
          'content-type': 'application/json; charset=utf-8',
          'Authorization': 'Bearer $token',
        },
        body: body,
      );
      responseJson = returnResponse(response);
    } on Exception {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  Future<dynamic> postAuthen(String url, dynamic body) async {
    dynamic responseJson;
    try {
      final response = await http.post(
        Uri.parse(_baseUrl + url),
        headers: <String, String>{
          'content-type': 'application/json; charset=utf-8',
        },
        body: body,
      );
      responseJson = returnResponse(response);
    } on Exception {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @visibleForTesting
  dynamic returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        // print(response.body.toString());
        dynamic responseJson = jsonDecode(response.body);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            'Error occured while communication with server' +
                ' with status code : ${response.statusCode}');
    }
  }
}
