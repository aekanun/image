import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/model/OrderHistoryModel.dart';

class OrderHistoryViewModel extends ChangeNotifier {
  OrderHistoryModel orderHistoryModel;

  DtoOrderHistory dto;
  OrderHistoryViewModel();
  OrderHistoryViewModel.instance() {
    this.orderHistoryModel = OrderHistoryModel.instance;
    this.orderHistoryModel.getDto(this);
    this.orderHistoryModel.getOrderHistory(this);
  }

  void setInitial(DtoOrderHistory dto) {
    this.dto = dto;
    notifyListeners();
  }

  void setOrderHistory(DtoOrderHistory dto) {
    this.dto = dto;
    notifyListeners();
  }
}
