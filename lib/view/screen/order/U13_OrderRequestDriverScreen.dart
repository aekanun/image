import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/assets.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/viewmodel/OrderViewModel.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

class OrderRequestDriverScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<OrderViewModel>(builder: (context, order, _) {
      return Scaffold(
        backgroundColor: Colors.white,
        // appBar: AppBar(
        //   centerTitle: true,
        //   title: Text("test load",
        //     style: TextStyle(
        //       color: Colors.white,
        //       letterSpacing: 1,
        //       fontSize: 20,
        //       fontWeight: FontWeight.w600,
        //     ),
        //   ),
        // ),
        body: Center(
          child: Padding(
            padding:
                const EdgeInsets.only(left: 16, bottom: 8, right: 16, top: 8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Order",
                  style: TextStyle(fontSize: 22),
                ),
                SizedBox(height: 20),
                Text(order.dto.orderSubmit != null &&
                        order.dto.orderSubmit.data.orderNo != null
                    ? "No." + order.dto.orderSubmit.data.orderNo.toString()
                    : "Process.."),
                SizedBox(height: 20),
                Text(order.dto.errorMessage == "submit order success"
                    ? "Request driver success"
                    : "Request driver..."),
                SizedBox(height: 20),
                Container(
                  child: order.dto.errorMessage == "submit order success"
                      ? Image.asset(Assets.check, height: 30, width: 30)
                      : SizedBox(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator()),
                ),
                SizedBox(height: 10),
                // Lottie.network(
                //   Assets.delivery_from_network,
                //   fit: BoxFit.fill,
                // ),
                Lottie.asset(
                  Assets.delivery,
                  // height: MediaQuery.of(context).size.height / 2,
                  // width: MediaQuery.of(context).size.width / 2,
                  fit: BoxFit.fill,
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            order.clearOrderSubmit();
            Navigator.pushReplacementNamed(context, u08);
          },
          child: const Icon(Icons.home),
          backgroundColor: Colors.purple,
          hoverElevation: 1.5,
          shape: StadiumBorder(side: BorderSide(color: Colors.blue, width: 4)),
          elevation: 1.5,
        ),
      );
    });
  }
}
