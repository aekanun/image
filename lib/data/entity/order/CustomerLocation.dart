import 'dart:convert';

CustomerLocation customerLocationFromJson(String str) => CustomerLocation.fromJson(json.decode(str));

String customerLocationToJson(CustomerLocation data) => json.encode(data.toJson());

class CustomerLocation {
  double lat;
  double long;

  CustomerLocation({this.lat, this.long});

  CustomerLocation.fromJson(Map<String, dynamic> json) {
    lat = json['lat'];
    long = json['long'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lat'] = this.lat;
    data['long'] = this.long;
    return data;
  }
}