import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/view/styles/ColorsConst.dart';
import 'package:laundry_delivery_app/viewmodel/OrderHistoryViewModel.dart';
import 'package:provider/provider.dart';

class OrderHistoryScreen extends StatefulWidget {
  OrderHistoryScreenState createState() => OrderHistoryScreenState();
}

class OrderHistoryScreenState extends State<OrderHistoryScreen> {
  @override
  void initState() {
    super.initState();
    print(
        '==============================OrderHistory init==============================');
  }

  @override
  void dispose() {
    print(
        '==============================OrderHistory dispose==============================');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<OrderHistoryViewModel>(builder: (context, orderHistory, _) {
      return Scaffold(
        backgroundColor: ColorsConst.white,
        appBar: AppBar(
          key: orderHistory.dto.scfdRootKey,
          title: Text(
            orderHistory.dto.title,
            style: TextStyle(color: Colors.black),
          ),
          bottomOpacity: 0.0,
          elevation: 0.0,
          backgroundColor: ColorsConst.white,
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              color: Colors.black,
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              child: orderHistory.dto.orderHistory == null ||
                      orderHistory.dto.orderHistory.data.length == 0
                  ? Center(child: Text('Empty'))
                  : ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: orderHistory.dto.orderHistory.data.length,
                      itemBuilder: (context, index) {
                        return Column(
                          children: <Widget>[
                            Container(
                              child: ListTile(
                                leading: Text((index + 1).toString()),
                                title: Text(orderHistory
                                    .dto.orderHistory.data[index].pickupTime),
                                subtitle: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('order: ' +
                                        orderHistory.dto.orderHistory
                                            .data[index].orderNo
                                            .toString()),
                                    Text('status: success')
                                  ],
                                ),
                                trailing: Text('฿ ' +
                                    orderHistory.dto.orderHistory.data[index]
                                        .totalCharge
                                        .toString()),
                              ),
                            )
                          ],
                        );
                      }),
            ),
            InkWell(
              child: Container(
                alignment: Alignment.center,
                height: 48.0,
                child: Text("Back",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18, color: Colors.purple)),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      );
    });
  }
}
