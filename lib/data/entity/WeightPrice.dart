class WeightPrice {
  int weight;
  int price;

  WeightPrice({this.weight, this.price});

  WeightPrice.fromJson(Map<String, dynamic> json) {
    weight = json['weight'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['weight'] = this.weight;
    data['price'] = this.price;
    return data;
  }
}