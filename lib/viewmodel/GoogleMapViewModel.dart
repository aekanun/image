import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:laundry_delivery_app/model/GoogleMapModel.dart';
import 'package:laundry_delivery_app/model/LaundryStoreModel.dart';

class GoogleMapViewModel with ChangeNotifier {
  GoogleMapModel _googleMapModel;
  LaundryStoreModel _laundryStoreModel;
  DtoGoogleMap dto;

  GoogleMapViewModel();
  GoogleMapViewModel.instance() {
    this._googleMapModel = GoogleMapModel.instance;
    this._laundryStoreModel = LaundryStoreModel.instance;
    this.dto = new DtoGoogleMap();
    getUserLocation();
  }

  void onGoogleMapCreated(GoogleMapController controller) {
    this._googleMapModel.onGoogleMapCreated(controller, this);
  }

  Future<void> getMoveCamera() async {
    this._googleMapModel.getMoveCamera();
  }

  Future<void> getUserLocation() async {
    this._googleMapModel.getUserLocation(googleMapViewModel: this);
  }

  void onCameraMove(CameraPosition position) async {
    this._googleMapModel.onCameraMove(position);
  }

  void setDto(DtoGoogleMap dto) {
    print('googlemapViewModel dto');
    this.dto = dto;
    this._laundryStoreModel.setCurrentLocation(
        currentAddress: this.dto.locationController.text,
        currentLatLng: new LatLng(this.dto.initialposition.latitude,
            this.dto.initialposition.longitude));
    notifyListeners();
  }

  void setLocationOnClick() {
    this._googleMapModel.setOwnLocation(this);
  }
}
