import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/data/entity/order_history/OrderHistory.dart';
import 'package:laundry_delivery_app/model/services/service.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/viewmodel/ViewModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrderHistoryModel {
  String _accessToken;
  Service _service;
  DtoOrderHistory _dto;
  OrderHistoryModel._privateConstructor() {
    SharedPreferences.getInstance().then((value) {
      this._accessToken = value.getString('accessToken') ?? '';
    });
    this._dto = new DtoOrderHistory();
    this._dto.title = "Order History";
    this._dto.scfdRootKey = new GlobalKey<ScaffoldState>();
    this._service = new Service();
  }

  static final OrderHistoryModel instance =
      OrderHistoryModel._privateConstructor();

  void getDto(OrderHistoryViewModel historyViewModel) {
    historyViewModel.setInitial(this._dto);
  }

  Future<void> getOrderHistory(OrderHistoryViewModel historyViewModel) async {
    this._dto.orderHistory = OrderHistory.fromJson(
        await this._service.get(url_order_history, this._accessToken));
    if (this._dto.orderHistory != null) {
      if (this._dto.orderHistory.code == 0) {
        this._dto.errorMessage = "success";
        historyViewModel.setOrderHistory(this._dto);
      } else {
        this._dto.errorMessage = "error";
        historyViewModel.setOrderHistory(this._dto);
      }
    } else {
      this._dto.errorMessage = "error";
      historyViewModel.setOrderHistory(this._dto);
    }
  }
}

class DtoOrderHistory {
  String errorMessage;
  String title;
  var scfdRootKey;
  OrderHistory orderHistory;

  DtoOrderHistory();
}
