class SocialTable {
  int id;
  String authenServer;
  String socialToken;

  SocialTable({this.id, this.authenServer, this.socialToken});

  SocialTable.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    authenServer = json['authenServer'];
    socialToken = json['socialToken'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['authenServer'] = this.authenServer;
    data['socialToken'] = this.socialToken;
    return data;
  }
}

