import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:laundry_delivery_app/model/OrderQueueModel.dart';
import 'package:laundry_delivery_app/model/model.dart';

class OrderQueueViewModel extends ChangeNotifier {
  OrderQueueModel _orderQueueModel;
  MqttModel _mqttModel;

  String errorMessage;

  DtoOrderQueue dto;
  DtoMqtt dtoMqtt;

// 1: World
// 5: Landmass/continent
// 10: City
// 15: Streets
// 20: Buildings
  double cameraZoom = 14;

  OrderQueueViewModel();
  OrderQueueViewModel.instance() {
    this._orderQueueModel = OrderQueueModel.instance;
    this._mqttModel = MqttModel.instance;
    this._orderQueueModel.getDto(this);
    this.dtoMqtt = this._mqttModel.data;
  }

  void getOrderQueue() {
    this._orderQueueModel.getOrderQueue(this);
  }

  void setInitial(DtoOrderQueue dto) {
    this.dto = dto;

    notifyListeners();
  }

  void setOrderQueue(DtoOrderQueue dto) {
    this.dto = dto;
    notifyListeners();
  }

  Future<void> callFromUrl(String url) async {
    this._orderQueueModel.callFromUrl("tel:$url", this);
  }

  void setErrorMessageFromCall(String errorMessage) {
    this.errorMessage = errorMessage;
    notifyListeners();
  }

  // void onCreated(GoogleMapController controller) {
  //   if (!mapController.isCompleted) {
  //     mapController.complete(controller);
  //   }
  //   notifyListeners();
  // }

  void setReceivedMsg(DtoMqtt dtoMqtt) {
    this.dtoMqtt = dtoMqtt;
    notifyListeners();
  }

  void discontect() {
    this.dtoMqtt = this._mqttModel.data;
    this._orderQueueModel.onRemoveMarker();
    this._mqttModel.disconnect();
  }

  void mqttContect(int index) {
    this._orderQueueModel.setIndexOrderTrackingTimeLine(index, this);
    this._mqttModel.initializeMQTTClient(
        orderQueueViewModel: this,
        merchantId: this
            .dto
            .orderQueue
            .data[index]
            .merchantDetail
            .merchantId
            .toString(),
        orderId: this.dto.orderQueue.data[index].orderNo.toString());
  }
}
