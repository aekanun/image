// {
//     "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcl9pZCI6NTgsInVzZXJfY3VzdG9tZXJfaWQiOiIiLCJjdXN0b21lcl9uYW1lIjoiYWVrYW51biIsImlhdCI6MTYyMzE0MTMyMywiZXhwIjoxNjIzMjI3NzIzfQ.fweyRhwt8hRmB_gfHiRnTHGHzTPC-9-RAVSeoQxCz-8",
//     "code": 0,
//     "msg_en": "Login success",
//     "msg_th": "เข้าระบบสำเร็จ",
//     "register": true,
//     "customer_id": 58,
//     "phon_number": ""
// }

// {
//     "code": 0,
//     "msg_en": "Plesae register",
//     "msg_th": "กรุณาลงทะเบียน",
//     "token": null,
//     "register": false,
//     "user_id": null
// }
class Authen {
  
  int code;
  String msgEn;
  String msgTh;
  String token;
  bool register;
  int userId;
  int phonnumber;

  Authen(
      {this.code,
      this.msgEn,
      this.msgTh,
      this.token,
      this.register,
      this.userId,
      this.phonnumber});

  Authen.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    msgEn = json['msg_en'];
    msgTh = json['msg_th'];
    token = json['token'];
    if(json['phone_number'] != null){
        phonnumber = json['phone_number'];
    }
    register = json['register'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['msg_en'] = this.msgEn;
    data['msg_th'] = this.msgTh;
    data['token'] = this.token;
    data['register'] = this.register;
    data['user_id'] = this.userId;
    data['phone_number']=this.phonnumber;
    return data;
  }
}
