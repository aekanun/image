import 'CustomerDetail.dart';
import 'DriverDetail.dart';
import 'MerchantDetail.dart';
import 'OrderDetail.dart';

class Data {
  int orderNo;
  String pickupTime;
  int orderStatus;
  CustomerDetail customerDetail;
  MerchantDetail merchantDetail;
  DriverDetail driverDetail;
  List<OrderDetail> orderDetail;
  int totalCharge;
  String paymentOption;

  Data(
      {this.orderNo,
      this.pickupTime,
      this.orderStatus,
      this.customerDetail,
      this.merchantDetail,
      this.driverDetail,
      this.orderDetail,
      this.totalCharge,
      this.paymentOption});

  Data.fromJson(Map<String, dynamic> json) {
    orderNo = json['order_no'];
    pickupTime = json['pickup_time'];
    orderStatus = json['order_status'];
    customerDetail = json['customer_detail'] != null
        ? new CustomerDetail.fromJson(json['customer_detail'])
        : null;
    merchantDetail = json['merchant_detail'] != null
        ? new MerchantDetail.fromJson(json['merchant_detail'])
        : null;
    driverDetail = json['driver_detail'] != null
        ? new DriverDetail.fromJson(json['driver_detail'])
        : null;
    if (json['order_detail'] != null) {
      orderDetail = new List<OrderDetail>();
      json['order_detail'].forEach((v) {
        orderDetail.add(new OrderDetail.fromJson(v));
      });
    }
    totalCharge = json['total_charge'];
    paymentOption = json['payment_option'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order_no'] = this.orderNo;
    data['pickup_time'] = this.pickupTime;
    data['order_status'] = this.orderStatus;
    if (this.customerDetail != null) {
      data['customer_detail'] = this.customerDetail.toJson();
    }
    if (this.merchantDetail != null) {
      data['merchant_detail'] = this.merchantDetail.toJson();
    }
    if (this.driverDetail != null) {
      data['driver_detail'] = this.driverDetail.toJson();
    }
    if (this.orderDetail != null) {
      data['order_detail'] = this.orderDetail.map((v) => v.toJson()).toList();
    }
    data['total_charge'] = this.totalCharge;
    data['payment_option'] = this.paymentOption;
    return data;
  }
}