import 'package:intl/intl.dart';
import 'package:laundry_delivery_app/data/entity/DryTempPrice.dart';
import 'package:laundry_delivery_app/data/entity/WaterTempPrice.dart';
import 'package:laundry_delivery_app/data/entity/WeightPrice.dart';

import 'Location.dart';

class Store {
  int merchantId;
  String name;
  String address;
  Location location;
  List<dynamic> workingTimes;
  List<WeightPrice> weightPrices;
  List<WaterTempPrice> waterTempPrices;
  List<DryTempPrice> dryTempPrices;

  Store(
      {this.merchantId,
      this.name,
      this.address,
      this.location,
      this.workingTimes,
      this.weightPrices,
      this.waterTempPrices,
      this.dryTempPrices});

  Store.fromJson(Map<String, dynamic> json) {
    merchantId = json['merchant_id'];
    name = json['name'];
    address = json['address'];
    location = json['location'] != null
        ? new Location.fromJson(json['location'])
        : null;
    if (json['working_time'] != null) {
      workingTimes = new List<dynamic>();
      json['working_time'].forEach((v) {
        workingTimes.add(v);
      });
    }
    if (json['weight_price'] != null) {
      weightPrices = new List<WeightPrice>();
      json['weight_price'].forEach((v) {
        weightPrices.add(new WeightPrice.fromJson(v));
      });
    }
    if (json['water_temp_price'] != null) {
      waterTempPrices = new List<WaterTempPrice>();
      json['water_temp_price'].forEach((v) {
        waterTempPrices.add(new WaterTempPrice.fromJson(v));
      });
    }
    if (json['dry_temp_price'] != null) {
      dryTempPrices = new List<DryTempPrice>();
      json['dry_temp_price'].forEach((v) {
        dryTempPrices.add(new DryTempPrice.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['merchant_id'] = this.merchantId;
    data['name'] = this.name;
    data['address'] = this.address;
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    if (this.workingTimes != null) {
      data['working_time'] = this.workingTimes.map((v) => v.toJson()).toList();
    }
    if (this.weightPrices != null) {
      data['weight_price'] = this.weightPrices.map((v) => v.toJson()).toList();
    }
    if (this.waterTempPrices != null) {
      data['water_temp_price'] =
          this.waterTempPrices.map((v) => v.toJson()).toList();
    }
    if (this.dryTempPrices != null) {
      data['dry_temp_price'] =
          this.dryTempPrices.map((v) => v.toJson()).toList();
    }
    return data;
  }

  get workingDay {
    if (this.workingTimes == null) return null;
    var date = DateTime.now();
    String time = _textSelect(this.workingTimes[date.weekday - 1].toString());
    if (time == "null") {
      return "ปิดทำการ";
    } else {
      String amPm = DateFormat('a').format(date);
      return time + " $amPm";
    }
  }

  bool openCloseStore() {
    if (this.workingTimes == null) return false;
    var date = DateTime.now();
    String time = _textSelect(this.workingTimes[date.weekday - 1].toString());

    if (time == "null") {
      return false;
    } else {
      DateTime now = DateTime.now();
      DateFormat formatter = DateFormat('yyyy-MM-dd');
      String startFormatted =
          formatter.format(now) + " " + time.substring(0, 5);
      String endFormatted = formatter.format(now) + " " + time.substring(6, 11);

      DateTime startDate = DateTime.parse(startFormatted);
      DateTime endDate = DateTime.parse(endFormatted);

      if (startDate.isBefore(now) && endDate.isAfter(now)) {
        return true;
      } else {
        return false;
      }
    }
  }

  String _textSelect(String str) {
    str = str.replaceAll(' ', '');
    str = str.replaceAll(',', '-');
    str = str.replaceAll('[', '');
    str = str.replaceAll(']', '');
    return str;
  }
}
