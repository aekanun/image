import 'dart:convert';

import 'package:laundry_delivery_app/data/DBProvider.dart';
import 'package:laundry_delivery_app/data/entity/ProfileFacebook.dart';
import 'package:laundry_delivery_app/data/entity/SocialTable.dart';
import 'package:laundry_delivery_app/model/services/service.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/viewmodel/viewModel.dart';

class SettingModel {
  DBProvider _db;
  Service _service;
  DtoSetting _dto;

  SettingModel._privateConstructor() {
    this._db = DBProvider.instance;
    this._service = new Service();
    this._dto = new DtoSetting();
  }
  static final SettingModel instance = SettingModel._privateConstructor();

  Future<void> getDataFromDb(SettingViewModel settingViewModel) async {
    final data = await this._db.queryAllRows();
    if (data.length > 0) {
      this._dto.socialTable = SocialTable.fromJson(data[0]);
      if (this._dto.socialTable.authenServer == facebook) {
        final graphResponse = await this._service.getGraphResponse(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture&access_token=${this._dto.socialTable.socialToken}');

        var profile = json.decode(graphResponse.body);
        this._dto.profileFacebook = ProfileFacebook.fromJson(profile);
        this
            ._dto
            .profileFacebook
            .setPictureUrl(profile['picture']['data']['url']);
        settingViewModel.setProfile(this._dto);
      }
    }
  }
}

class DtoSetting {
  SocialTable socialTable;
  ProfileFacebook profileFacebook;
  DtoSetting();
}
