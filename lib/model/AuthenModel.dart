import 'dart:convert';

import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:laundry_delivery_app/data/DBProvider.dart';
import 'package:laundry_delivery_app/data/entity/Authen.dart';
import 'package:laundry_delivery_app/data/entity/FacebookResponse.dart';
import 'package:laundry_delivery_app/data/entity/GoogleResponse.dart';
import 'package:laundry_delivery_app/data/entity/ProfileFacebook.dart';
import 'package:laundry_delivery_app/model/services/service.dart';
import 'package:laundry_delivery_app/utility/enums.dart';
import 'package:laundry_delivery_app/viewmodel/ViewModel.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthenModel {
  static GoogleSignIn googleSignIn; //Can be used both platform
  static FacebookLogin facebookSignIn;
  DtoAuthen _dto;
  DBProvider db;
  Service service;
  LoadingState loadingState;
  SharedPreferences _prefs;

  AuthenModel._privateConstructor() {
    googleSignIn = GoogleSignIn(clientId: google_client_id);
    facebookSignIn = new FacebookLogin();
    this._dto = new DtoAuthen();
    this.service = new Service();
    this.loadingState = LoadingState.none;
    this.db = DBProvider.instance;
    SharedPreferences.getInstance().then((value) {
      _prefs = value;
    });
  }
  static final AuthenModel instance = AuthenModel._privateConstructor();

  setLoginData(Map mapController, AuthenViewModel authenViewModel) {
    if (this.loadingState == LoadingState.none) {
      this.loadingState = LoadingState.loading;
      this._dto.email = mapController['email'];
      this._dto.password = mapController['password'];
      this._dto.dataEntry = false;
      if (this._dto.email.isNotEmpty && this._dto.password.isNotEmpty) {
        this._dto.dataEntry = true;
        userLogin(authenViewModel);
      } else {
        this.loadingState = LoadingState.none;
        authenViewModel.setDto(this._dto);
      }
    }
  }

  Future<void> userLogin(AuthenViewModel authenViewModel) async {
    dynamic body = jsonEncode(<String, String>{
      'authen_server': basic_authen,
      'username': this._dto.email,
      'password': this._dto.password,
    });
    print(body.toString());
     this._dto.authen =
         Authen.fromJson(await service.postAuthen(url_customer_login, body));
     if (this._dto.authen != null) {
      if (this._dto.authen.code == 0) {
         this._dto.errorMessage = "login success";
         this.loadingState = LoadingState.none;
        // await _prefs.setString("accessToken", this._dto.authen.token);
         authenViewModel.setDto(this._dto);
       } else {
         this._dto.errorMessage = "user login fail";
         this.loadingState = LoadingState.none;
         authenViewModel.setDto(this._dto);
       }
     } else {
       this._dto.errorMessage = "user login fail";
       this.loadingState = LoadingState.none;
       authenViewModel.setDto(this._dto);
     }
  }

  Future<void> socialLogin(
      AuthenViewModel authenViewModel, String authenServer) async {
    dynamic body = jsonEncode(<String, String>{
      'authen_server': authenServer,
      'access_token': this._dto.authen.token,
      'email': this._dto.email,
    });
    dynamic row = {
      'authenServer': authenServer,
      'socialToken': this._dto.authen.token
    };
    final id = await this.db.insert(row);
    print('==============inserted row id: $id');
    print("===========socialLogin: " + body.toString());
    this._dto.authen =
        Authen.fromJson(await service.postAuthen(url_customer_login, body));
    if (this._dto.authen != null) {
      if (this._dto.authen.code == 0) {
        if (this._dto.authen.register) {
          this._dto.errorMessage = "login success";
          this.loadingState = LoadingState.none;
          await _prefs.setString("accessToken", this._dto.authen.token);
          authenViewModel.setDto(this._dto);
        } else {
          this._dto.errorMessage = "social login fail";
          this.loadingState = LoadingState.none;
          authenViewModel.setDto(this._dto);
        }
      } else {
        this._dto.errorMessage = "social login connect fail";
        this.loadingState = LoadingState.none;
        authenViewModel.setDto(this._dto);
      }
    } else {
      this._dto.errorMessage = "social login connect fail";
      this.loadingState = LoadingState.none;
      authenViewModel.setDto(this._dto);
    }
  }

  Future<void> loginWithFacebook(AuthenViewModel authenViewModel) async {
    FacebookLoginResult result = await facebookSignIn.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        this._dto.errorMessage = "login success";
        this._dto.serviceName = facebook;
        this._dto.facebookResponse = FacebookResponse.fromMap(result);
        this._dto.authen.token = this._dto.facebookResponse.accessToken.token;
        final graphResponse = await service.getGraphResponse(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${this._dto.authen.token}');
        this._dto.profileFacebook =
            ProfileFacebook.fromJson(json.decode(graphResponse.body));
        this._dto.email = this._dto.profileFacebook.email;
        socialLogin(authenViewModel, facebook);
        break;
      case FacebookLoginStatus.cancelledByUser:
        this._dto.errorMessage = "error";
        this._dto.serviceName = facebook;
        this._dto.facebookResponse = FacebookResponse.fromMap(null);
        authenViewModel.setDto(this._dto);
        break;
      case FacebookLoginStatus.error:
        this._dto.errorMessage = "error";
        this._dto.serviceName = facebook;
        this._dto.facebookResponse = FacebookResponse.fromMap(null);
        authenViewModel.setDto(this._dto);
        break;
    }
  }

  Future<Null> logoutFacebook() async {
    await facebookSignIn.logOut();
  }

  Future<void> loginWithGmail(AuthenViewModel authenViewModel) async {
    await googleSignIn.signIn();
    accessData(authenViewModel);
  }

  accessData(AuthenViewModel authenViewModel) {
    googleSignIn.signIn().then((result) {
      result.authentication.then((googleKey) async {
        this._dto.errorMessage = "login success";
        this._dto.serviceName = google;
        this._dto.googleResponse =
            GoogleResponse.fromMap(googleSignIn, googleKey);
        this._dto.authen.token = this._dto.googleResponse.googleKey.accessToken;
        this._dto.email =
            this._dto.googleResponse.googleSignIn.currentUser.email;
        socialLogin(authenViewModel, google);
      }).catchError((err) {
        print('inner error');
        this._dto.errorMessage = "error";
        this._dto.serviceName = google;
        this._dto.googleResponse = GoogleResponse.fromMap(null, null);
        authenViewModel.setDto(this._dto);
      });
    }).catchError((err) {
      print('error occured');
      this._dto.errorMessage = "error";
      this._dto.serviceName = google;
      this._dto.googleResponse = GoogleResponse.fromMap(null, null);
      authenViewModel.setDto(this._dto);
    });
  }

  Future<void> currentUserChange() async {
    googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      print(account.displayName);
      print(account.email);
    });
    googleSignIn.signInSilently();
  }

  Future<bool> isSignedInWithGmail() async {
    bool signed = await googleSignIn.isSignedIn();
    return signed;
  }

  Future<void> logoutWithGmail() async {
    await googleSignIn.signOut();
  }

  Future<void> logout() async {
    await _prefs.remove('accessToken');
    await this.db.deleteAll();
    this._dto.currentNavigation = '';
  }

  void updateNavigation(String navigation, AuthenViewModel authenViewModel) {
    this._dto.currentNavigation = navigation;
    authenViewModel.updateNavigation(this._dto);
  }

  DtoAuthen get getDto => this._dto;
}

class DtoAuthen {
  //user login
  String email;
  String password;

  bool dataEntry;
  String errorMessage;
  String serviceName;
  FacebookResponse facebookResponse;
  GoogleResponse googleResponse;
  Authen authen = new Authen();
  ProfileFacebook profileFacebook = new ProfileFacebook();

  String currentNavigation = '';

  DtoAuthen();
}
